<?php

use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

User::create([
	'name'           => 'Atul Kumar',
	'email'          => 'a@a.com',
	'password'       => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
]);

$roles = ['Super Admin', 'Admin', 'Manager', 'Team Leader', 'Counsellor'];

foreach ($roles as $role) {
	Role::create(['name' => $role]);
}

$permissions = ['create user', 'edit user', 'delete user', 'create domain', 'edit domain', 'delete domain', 'view logs'];

foreach ($permissions as $permission) {
	Permission::create(['name' => $permission]);
}

$superAdminRole = Role::findByName('Super Admin');
$superAdminRole->syncPermissions($permissions);

$adminRole = Role::findByName('Admin');
$adminRole->syncPermissions(['create user', 'edit user', 'create domain', 'edit domain', 'view logs']);

$managerRole = Role::findByName('Manager');
$managerRole->syncPermissions(['create user', 'edit user', 'view logs']);

$teamLeaderRole = Role::findByName('Team Leader');
$teamLeaderRole->syncPermissions(['create user', 'edit user']);

$counsellorRole = Role::findByName('Counsellor');
$counsellorRole->syncPermissions(['view logs']);
