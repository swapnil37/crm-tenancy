<?php

namespace App\Helpers;

use Spatie\LaravelSettings\Settings;

class GeneralSettings extends Settings
{
	public string $site_name;
	public string $site_active;
	public string $email_api;
	public string $email_api_key;
	public string $email_secret_key;
	public string $sms_api_key;
	public string $payment_api_key;
	public string $storage_type;


	public static function group(): string
	{
		return 'general';
	}
}
