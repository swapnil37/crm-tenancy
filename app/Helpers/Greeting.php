<?php

namespace App\Helpers;

use Carbon\Carbon;

class Greeting extends Carbon
{
    public static function show(): string
    {
        $hour = (new Carbon)->format('H');

        if ($hour < 12) {
            return 'Good morning';
        }

        if ($hour < 18) {
            return 'Good afternoon';
        }

        return 'Good evening';
    }
}
