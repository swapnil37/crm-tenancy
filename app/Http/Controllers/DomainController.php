<?php

namespace App\Http\Controllers;

use App\Jobs\SendDomainCreationMail;
use App\Models\Tenant;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Stancl\Tenancy\Database\Models\Domain;

class DomainController extends Controller
{
	public function index(Request $request)
	{
		if ($request->ajax()) {
			return Domain::with('tenant:id,data')->get(['id', 'domain', 'tenant_id', 'created_at']);
		}

		return view('central.domain');
	}

	public function store(Request $request)
	{
		$validated = $request->validate([
			'domain'       => 'required|string',
			'organisation' => 'required|string',
			'first_name'   => 'required|string',
			'last_name'    => 'required|string',
			'email'        => 'email',
			'phone'        => 'required|string',
		]);



		$tenant = Tenant::create();

		$newTenant = $tenant->createDomain([
			'domain' => $validated['domain'],
		]);

		$tenant->update([
			'organisation' => $validated['organisation'],
			'first_name'   => $validated['first_name'],
			'last_name'    => $validated['last_name'],
			'email'        => $validated['email'],
			'phone'        => $validated['phone'],
		]);

		tenancy()->initialize($tenant);



		$plainTextPassword = bin2hex(random_bytes(4));

		SendDomainCreationMail::dispatch($validated['email'],$validated['domain'],$validated['organisation'],$plainTextPassword);

		User::create([
			'first_name'  => $validated['first_name'],
			'last_name'   => $validated['last_name'],
			'email'       => $validated['email'],
			'phone'       => $validated['phone'],
			'password'    => Hash::make($plainTextPassword),
		]);

		//TODO Add Data to Send


		return $newTenant;
	}

	public function show($id)
	{
		//
	}

	public function edit($id)
	{
		$domains = Domain::with('tenant:id,data')->get(['id', 'domain', 'tenant_id', 'created_at'])->find($id);
        return $domains;
	}

	public function update(Request $request, $id)
	{
		//
	}

	public function destroy($id)
	{
		//
	}
}
