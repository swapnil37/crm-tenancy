<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmailTemplate;

class EmailTemplateController extends Controller
{
	public function index(Request $request)
	{
		if ($request->ajax()) {
			return EmailTemplate::all();
		}

		return view('central.email-template');
	}

	public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'subject' => 'required|string',
            'template' => 'required|string',
        ]);

        return EmailTemplate::create([
            'name' => $validated['name'],
            'subject' => $validated['subject'],
            'template' => $validated['template'],
        ]);
    }

	public function edit($id)
	{
		$emailTemplate = EmailTemplate::findOrFail($id);
        return $emailTemplate;
	}

	public function destroy(Request $request){
		$emailTemplate = EmailTemplate::findOrFail($request)->first();
		return $emailTemplate->delete();
	}
}
