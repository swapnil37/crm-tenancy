<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function index(Request $request)
    {
        return Permission::all();
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'  => 'required|string|unique:permissions',
            'roles' => 'required|array',
        ]);

        $permission = Permission::create(['name' => $validated['name']]);

        foreach ($validated['roles'] as $roleName) {
            if ($role = Role::findByName($roleName)) {
                $permission->assignRole($role);
            }
        }
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
