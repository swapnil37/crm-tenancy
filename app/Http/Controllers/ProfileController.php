<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
	public function index(Request $request)
	{
		if ($request->ajax()) {
			$user = auth()->user();

			return [
				'first_name' => $user->first_name,
				'last_name'  => $user->last_name,
				'email'      => $user->email,
				'phone'      => $user->phone,
			];
		}

		return view('central.profile');
	}

	public function update(Request $request)
	{
		$validated = $request->validate([
			'first_name' => 'required|string',
			'last_name'  => 'required|string',
			'email'      => 'required|email',
			'phone'      => 'string',
		]);

		return User::find(Auth::id())
		           ->update([
			           'first_name' => $validated['first_name'],
			           'last_name'  => $validated['last_name'],
			           'email'      => $validated['email'],
			           'phone'      => $validated['phone'],
		           ]);
	}
}
