<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index(Request $request)
    {
    	if ($request->ajax()) {
            return Role::where('name', '!=', 'Super Admin')
                       ->with('permissions')
                       ->get(['id', 'name', 'updated_at']);
        }
        return view('central.role');

    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string|unique:roles',
        ]);

        return Role::create([
            'name' => $validated['name'],
        ]);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
