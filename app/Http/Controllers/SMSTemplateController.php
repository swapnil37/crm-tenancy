<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SMSTemplate;

class SMSTemplateController extends Controller
{
    public function index(Request $request)
	{
		if ($request->ajax()) {
			return SMSTemplate::all();
		}

		return view('central.sms-template');
	}

	public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'template' => 'required|string',
        ]);

        return SMSTemplate::create([
            'name' => $validated['name'],
            'template' => $validated['template'],
        ]);
    }

	public function edit($id)
	{
		//validate
		$smsTemplate = SMSTemplate::findOrFail($id);
        return $smsTemplate;
	}

	public function destroy(Request $request){
		//validate


		$smsTemplate = SMSTemplate::findOrFail($request)->first();
		return $smsTemplate->delete();
	}
}
