<?php

namespace App\Http\Controllers;

use App\Helpers\GeneralSettings;
use Illuminate\Http\Request;

class SettingController
{
	public function index(Request $request, GeneralSettings $settings)
	{
		if ($request->ajax()) {
			return $settings->toArray();
		}

		return view('central.setting');
	}

	public function update(Request $request, GeneralSettings $settings)
	{

		$validated = $request->validate([
			'site_name' => 'required|string',
			'site_active'=> 'required|string',
			'email_api'=> 'required|string',
			'email_api_key'=> 'required|string',
			'email_secret_key'=> 'string',
			'sms_api_key' => 'required|string',
			'payment_api_key'=> 'required|string',
			'storage_type'=>'required|string|',
		]);



		$settings->site_name = $validated['site_name'];
		$settings->site_active = $validated['site_active'];
		$settings->email_service = $validated['email_api'];
		$settings->email_api_key = $validated['email_api_key'];
		$settings->email_secret_key = $validated['email_secret_key'];
		$settings->sms_api_key = $validated['sms_api_key'];
		$settings->payment_api_key = $validated['payment_api_key'];
		$settings->storage_type = $validated['storage_type'];
		$settings->save();

		return $settings;
	}
}
