<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WhatsappTemplate;

class WhatsappTemplateController extends Controller
{
    public function index(Request $request)
	{
		if ($request->ajax()) {
			return WhatsappTemplate::all();
		}

		return view('central.whatsapp-template');
	}

	public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'template' => 'required|string',
        ]);

        return WhatsappTemplate::create([
            'name' => $validated['name'],
            'template' => $validated['template'],
        ]);
    }

	public function edit($id)
	{
		$whatsappTemplate = WhatsappTemplate::find($id);
        return $whatsappTemplate;
	}

	public function destroy(Request $request){
		$whatsappTemplate = WhatsappTemplate::find($request)->first();
		return $whatsappTemplate->delete();
	}
}
