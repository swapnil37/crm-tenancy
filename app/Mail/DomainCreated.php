<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DomainCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct(
		protected string $email,
		protected string $domain,
		protected string $organisation,
		protected string $password,
	){}

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('CRM created successfully!')->view('email_template.domaincreated')
		->with('email', $this->email)
		->with('domain', $this->domain)
		->with('organisation', $this->organisation)
		->with('password', $this->password);
    }
}
