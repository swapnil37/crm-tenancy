<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MetricCard extends Component
{
    public function __construct(
        public string $icon,
        public string $heading,
        public string $metric,
        public string $route,
    ) {}

	public function render()
	{
		return view('components.metric-card');
	}
}
