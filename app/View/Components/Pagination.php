<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Pagination extends Component
{
    public function __construct(
        public int $cursorStart,
        public int $cursorEnd,
        public int $total,
    ) {}

    public function render()
    {
        return view('components.pagination');
    }
}
