require('./bootstrap');

require('./components/central/Domain')
require('./components/central/Profile')
require('./components/central/Role')
require('./components/central/Setting')
require('./components/central/Notification')
require('./components/central/User')
require('./components/central/Email-Template')
require('./components/central/SMS-Template')
require('./components/central/Whatsapp-Template')
