import React, {useState, useEffect} from "react"
import {useForm} from "react-hook-form"
import {render} from "react-dom"

import axios from "axios";
import ReactDataGrid from '@inovua/reactdatagrid-enterprise'
import '@inovua/reactdatagrid-enterprise/index.css'
import { ToastProvider, useToasts } from 'react-toast-notifications'
import { PencilAltIcon, TrashIcon } from '@heroicons/react/outline'

import ErrorSVG from "../common/ErrorSVG"

function Domain(){
    const { addToast } = useToasts();
    const [showForm, setShowForm] = useState(false)
	const [isEditing, setIsEditing] = useState(false)
    const [domains, setDomains] = useState([])
    const {register, handleSubmit, setValue, reset, formState: {errors}} = useForm()
    const gridStyle = { minHeight: 700 }
    const columns = [
	    {
		    name: 'tenant_id',
		    header: 'Identifier',
		    maxWidth: 1000,
		    defaultFlex: 1,
		    defaultVisible: false
	    },
        {
        	name: 'domain',
	        header: 'SubDomain',
	        minWidth: 100,
	        defaultFlex: 1,
	        render: ({value}) => value + '.edutra.io'
        },
	    {
        	name: 'organisation',
	        header: 'Organisation',
	        minWidth: 50,
	        defaultFlex: 1,
		    render: ({ data }) => data.tenant.organisation
        },
	    {
		    name: 'email',
		    header: 'Admin Email',
		    maxWidth: 1000,
		    defaultFlex: 1,
		    render: ({ data }) => data.tenant.email
	    },
	    {
		    name: 'contact',
		    header: 'Contact Person',
		    maxWidth: 1000,
		    defaultFlex: 1,
		    defaultVisible: false,
		    render: ({ data }) => data.tenant.first_name + ' ' + data.tenant.last_name
	    },
	    {
		    name: 'phone',
		    header: 'Phone Number',
		    maxWidth: 1000,
		    defaultFlex: 1,
		    defaultVisible: false,
		    render: ({ data }) => data.tenant.phone
	    },
        {
            name: 'created_at',
            header: 'Created',
            maxWidth: 1000,
            defaultFlex: 1,
            render: ({ value }) => (new Date(value)).toDateString() + ' ' +(new Date(value)).toLocaleTimeString()
        },
		{
        	name: 'id',
	        header: 'Action',
	        defaultWidth: 80,
	        defaultFlex: 1,
            render: ({ value }) => <div className="grid grid-cols-8 space-x-1 text-right"><div><PencilAltIcon className="w-4 h-4 text-indigo-700 cursor-pointer" onClick={editDomain(value)} /></div> </div>
        }

    ]

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios(
                '/user/domains',
            )
            setDomains(result.data)
        };

        fetchData()
    }, [])



    const onSubmit = (data) => {
		const url = isEditing ? '/user/domains/update' : '/user/domains'
        axios.post(url, {
            domain: data.domain+'.crm.test',
            organisation: data.organisation,
            first_name: data.first_name,
            last_name: data.last_name,
            email: data.email,
	        phone: data.phone,
        }).then((response) => {
	        addToast('Saved Successfully', { appearance: 'success' });
            setDomains([...domains, response.data])
            setShowForm(false)
        }).catch(function (error) {
            addToast(error.message, { appearance: 'error' });
            console.log(error)
        });
    }

	const editDomain = value => () => {
		axios.get('/user/domains/edit/'+value, {
        }).then((response) => {
			const values = response.data
            setShowForm(true)
			setIsEditing(true)
			setValue('domain', values.domain);
			setValue('organisation', values.tenant.organisation);
			setValue('first_name', values.tenant.first_name);
			setValue('last_name', values.tenant.last_name);
			setValue('email', values.tenant.email);
			setValue('phone', values.tenant.phone);
        }).catch(function (error) {
            addToast(error.message, { appearance: 'error' });
            console.log(error)
        });
	};

    return (
        <div>
            <div className="pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
                <h3 className="text-lg font-medium leading-6 text-gray-900">
                    Domains
                </h3>
                <div className="flex mt-3 space-x-3 sm:mt-0 sm:ml-4">
                    <button type="button" className="tw-btn tw-btn-green" onClick={() => {setShowForm(true); reset();}}>Create</button>
                    <button type="button" className="tw-btn tw-btn-indigo" onClick={() => setShowForm(false)}>View</button>
                </div>
            </div>

	        <div className="p-4 mb-4 rounded-md bg-cyan-600">
		        <div className="flex">
			        <div className="flex-shrink-0">
				        <svg className="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
					        <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clipRule="evenodd" />
				        </svg>
			        </div>
			        <div className="ml-3 text-sm font-medium text-white">
				        Full contact detail can be viewed on demand by clicking on the header row and selecting Columns.
			        </div>
		        </div>
	        </div>

            {
                !showForm &&
                <ReactDataGrid
                    idProperty="id"
                    columns={columns}
                    dataSource={domains}
                    style={gridStyle}
                />
            }

            {
                showForm &&
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="mt-2 space-y-6">
                        <div>
                            <div className="tw-form-section-wrapper">
                                <div className="md:grid md:grid-cols-3 md:gap-6">
                                    <div className="md:col-span-1">
                                        <h3 className="tw-form-section-heading">Domain</h3>
                                        <p className="tw-form-section-subheading">
                                            This will be the main url for the website.
                                        </p>
                                    </div>

                                    <div className="mt-5 md:mt-0 md:col-span-2">
                                        <div className="space-y-6">
                                            <div className="grid grid-cols-3 gap-6">
                                                <div className="col-span-3 sm:col-span-2">
                                                    <label htmlFor="domain" className="tw-form-label">
                                                        Website
                                                    </label>
                                                    <div className="flex mt-1 rounded-md shadow-sm">
                                                        <span className="tw-form-input-prefix">https://</span>
                                                        <span className="relative">
                                                            <input {...register("domain", {required: true})}
                                                                   className="mt-0 rounded-l-none rounded-r-none tw-form-input"
                                                                   placeholder="subdomain"

                                                            />
                                                            {errors.domain && <ErrorSVG />}
                                                        </span>
                                                        <span className="font-bold bg-yellow-100 tw-form-input-suffix">.edutra.io</span>
                                                    </div>
                                                    {errors.domain && <p className="tw-form-input-error">Invalid subdomain name.</p>}
                                                </div>
                                            </div>

	                                        <div>
		                                        <label htmlFor="role" className="tw-form-label">
			                                        Name of Organisation
		                                        </label>
		                                        <div className="relative">
			                                        <div className="relative flex mt-1 rounded-md shadow-sm">
				                                        <input {...register("organisation", {required: true})}
				                                               className="tw-form-input"
				                                        />
				                                        {errors.organisation && <ErrorSVG />}
			                                        </div>
			                                        {errors.organisation && <p className="tw-form-input-error">Invalid organisation.</p>}
		                                        </div>
	                                        </div>

	                                        <div>
		                                        <label htmlFor="role" className="tw-form-label">
			                                        First name
		                                        </label>
		                                        <div className="relative">
			                                        <div className="relative flex mt-1 rounded-md shadow-sm">
				                                        <input {...register("first_name", {required: true})}
				                                               className="tw-form-input"
				                                        />
				                                        {errors.first_name && <ErrorSVG />}
			                                        </div>
			                                        {errors.first_name && <p className="tw-form-input-error">Invalid name.</p>}
		                                        </div>
	                                        </div>

	                                        <div>
		                                        <label htmlFor="role" className="tw-form-label">
			                                        Last name
		                                        </label>
		                                        <div className="relative">
			                                        <div className="relative flex mt-1 rounded-md shadow-sm">
				                                        <input {...register("last_name", {required: true})}
				                                               className="tw-form-input"
				                                        />
				                                        {errors.last_name && <ErrorSVG />}
			                                        </div>
			                                        {errors.last_name && <p className="tw-form-input-error">Invalid name.</p>}
		                                        </div>
	                                        </div>

	                                        <div>
		                                        <label htmlFor="role" className="tw-form-label">
			                                        Admin Email
		                                        </label>
		                                        <div className="relative">
			                                        <div className="relative flex mt-1 rounded-md shadow-sm">
				                                        <input {...register("email", {required: true})}
				                                               className="tw-form-input"
				                                        />
				                                        {errors.permission && <ErrorSVG />}
			                                        </div>
			                                        {errors.permission && <p className="tw-form-input-error">Invalid email.</p>}
		                                        </div>
	                                        </div>

	                                        <div>
		                                        <label htmlFor="role" className="tw-form-label">
			                                        Phone
		                                        </label>
		                                        <div className="relative">
			                                        <div className="relative flex mt-1 rounded-md shadow-sm">
				                                        <input {...register("phone", {required: true})}
				                                               className="tw-form-input"
				                                        />
				                                        {errors.phone && <ErrorSVG />}
			                                        </div>
			                                        {errors.phone && <p className="tw-form-input-error">Invalid phone.</p>}
		                                        </div>
	                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="tw-form-button-wrapper">
                                <button type="button" className="tw-btn tw-btn-white" onClick={() => setShowForm(false)}>Cancel</button>
                                <button type="submit" className="tw-btn tw-btn-indigo">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            }

        </div>
    )
}

if (document.getElementById('domain')) {
    render(<ToastProvider autoDismiss="true"><Domain /></ToastProvider>, document.getElementById('domain'))
}
