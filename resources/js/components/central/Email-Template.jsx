import React, { useState, useEffect, Fragment, useRef } from 'react';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {useForm} from "react-hook-form"
import axios from "axios";
import ReactDataGrid from '@inovua/reactdatagrid-enterprise'
import '@inovua/reactdatagrid-enterprise/index.css'
import { ToastProvider, useToasts } from 'react-toast-notifications'
import { Dialog, Transition } from '@headlessui/react'
import { PencilAltIcon, TrashIcon, EyeIcon, ExclamationIcon } from '@heroicons/react/outline'
import ReactHtmlParser from 'react-html-parser';



import {render} from "react-dom";



function Email(){
	const { addToast } = useToasts();
	const [showTemplateForm, setShowTemplateForm] = useState(false)
	const [showTemplate, setShowTemplate] = useState(false)
	const [isEditing, setIsEditing] = useState(false)
	const [Editor, setEditor] = useState('')
	const [addData, setVal] = useState('')
	const [templates, setTepmlates] = useState([])
	const {register, handleSubmit, setValue, reset, formState: {errors}} = useForm()
	const [open, setOpen] = useState(false)
	const cancelButtonRef = useRef()
	const [delValue, setDelValue] = useState('')
	const gridStyle = { minHeight: 700 }
    const columns = [
        {
        	name: 'name',
	        header: 'Template Name',
	        minWidth: 200,
	        defaultFlex: 1,
	        render: ({value}) => value
        },
		{
		    name: 'subject',
		    header: 'Subject',
		    maxWidth: 500,
		    defaultFlex: 1,
		    render: ({ value }) => value
	    },
	    {
        	name: 'template',
	        header: 'Template',
	        minWidth: 550,
	        defaultFlex: 1,
		    render: ({ value }) => value,
		    defaultVisible: false
        },
		{
        	name: 'id',
	        header: 'Action',
	        maxWidth: 140,
	        defaultFlex: 1,
            render: ({ value }) => <div className="grid grid-cols-5 space-x-1"><div><EyeIcon className="w-4 h-4 text-indigo-700 cursor-pointer" onClick={viewTemplate(value)} /></div> <div><PencilAltIcon className="w-4 h-4 text-indigo-700 cursor-pointer" onClick={editTemplate(value)} /></div> <div><TrashIcon className="w-4 h-4 text-red-700 cursor-pointer" onClick={destroyTemplate(value)}/></div></div>
        }

    ]

	function onEditorChange(evt, editor) {
		setEditor(
			editor.getData()
		)
    }



	const onSubmit = (data) => {
		const template = document.getElementById('')
		const url = isEditing ? '/user/email-template/update' : '/user/email-template'
        axios.post(url, {
            name: data.name,
            subject: data.subject,
            template: Editor,
        }).then((response) => {
	        addToast('Saved Successfully', { appearance: 'success' });
            setTepmlates([...templates, response.data])
            setShowTemplateForm(false)
        }).catch(function (error) {
            addToast(error.message, { appearance: 'error' });
            console.log(error)
        });
    }

	const viewTemplate = value => () => {
		axios.get('/user/email-template/edit/'+value, {
        }).then((response) => {
			const values = response.data
			setVal(values.template);
            setShowTemplate(true)
        }).catch(function (error) {
            addToast(error.message, { appearance: 'error' });
            console.log(error)
        });
	};

	const editTemplate = value => () => {
		axios.get('/user/email-template/edit/'+value, {
        }).then((response) => {
			setIsEditing(true)
			const values = response.data
            setShowTemplateForm(true)
			setValue('name', values.name);
			setValue('subject', values.subject);
			editor.setData(values.template);
        }).catch(function (error) {
            addToast(error.message, { appearance: 'error' });
            console.log(error)
        });
	};

	const destroyTemplate = value => () => {
		setOpen(true)
		setDelValue(value)
	};

	const deleteValue = open ? delValue : ''
	const deleteTemplate = () => {
		axios.post('/user/email-template/delete/', {
			id: deleteValue
		}).then((response) => {
			if(response.data==1){
				setOpen(false)
				addToast('Deleted successfully!', { appearance: 'success' } );
				const fetchData = async () => {
					const result = await axios(
						'/user/email-template',
					)
					setTepmlates(result.data)
				};
				fetchData()
			}
		}).catch(function (error) {
			addToast(error.message, { appearance: 'error' });
			console.log(error)
		});
	}

	useEffect(() => {
        const fetchData = async () => {
            const result = await axios(
                '/user/email-template',
            )

			setTepmlates(result.data)
        };

        fetchData()
    }, [])

        return (
            <div className="App">
				<div className="pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
					<h3 className="text-lg font-medium leading-6 text-gray-900">
						Email Templates
					</h3>
					<div className="flex mt-3 space-x-3 sm:mt-0 sm:ml-4">
						<button type="button"
								className="tw-btn tw-btn-green"
								onClick={() => {setShowTemplateForm(true); reset();}}
						>
							Create
						</button>
						<button type="button" className="tw-btn tw-btn-indigo" onClick={() => {setShowTemplateForm(false); setShowTemplate(false);}}>View</button>
					</div>
				</div>

				{
					!showTemplateForm &&
					!showTemplate &&
					<ReactDataGrid
						idProperty="id"
						columns={columns}
						dataSource={templates}
						style={gridStyle}
					/>
				}
				{
				showTemplateForm &&
				<form onSubmit={handleSubmit(onSubmit)}>
					<div className="tw-form-section-wrapper">
						<div className="mt-5 mb-8 md:mt-0">
							<div className="grid grid-cols-2 gap-6">

								<div>
									<label>Template Name</label>
									<input {...register("name", {required: true})}
										className="mt-0 tw-form-input"
										placeholder="e.g. Thanks for Signup"
									/>
								</div>

								<div>
									<label>Subject</label>
									<input {...register("subject", {required: true})}
										className="mt-0 tw-form-input"
										placeholder="e.g. Welcome to Edutra"
									/>
								</div>

								<div>
									<label>Trigger Condition</label>
									<select {...register("trigger", { required: true })}
										className="mt-0 rounded-md shadow-sm tw-form-select"
									>
										<option value="signup">New User Added</option>
										<option value="forgot">Forgot Password</option>
									</select>
								</div>

								<div>
									<label>Variables</label>
									<select
										className="mt-0 rounded-md shadow-sm tw-form-select"
									>
										<option value="name">Name</option>
										<option value="email">Email</option>
										<option value="password">Password</option>
										<option value="org_name">Organization Name</option>
									</select>
								</div>

							</div>
						</div>

						<CKEditor

							editor={ ClassicEditor }
							data=""
							onChange={onEditorChange}

							//TODO: Choose Variable OnSelect Change
							//TODO: CKEditor Image Uploading https://ckeditor.com/docs/ckeditor5/latest/features/image-upload/simple-upload-adapter.html
						/>

					</div>
					<div className="tw-form-button-wrapper">
						<button type="button"
								className="tw-btn tw-btn-white"
								onClick={() => setShowTemplateForm(false)}
						>
							Cancel
						</button>
						<button type="submit" className="tw-btn tw-btn-indigo">Save</button>
					</div>
				</form>

				}
				{
					<Transition.Root show={open} as={Fragment}>
					<Dialog
						as="div"
						static
						className="fixed inset-0 z-10 overflow-y-auto"
						initialFocus={cancelButtonRef}
						open={open}
						onClose={setOpen}
					>
						<div className="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
						<Transition.Child
							as={Fragment}
							enter="ease-out duration-300"
							enterFrom="opacity-0"
							enterTo="opacity-100"
							leave="ease-in duration-200"
							leaveFrom="opacity-100"
							leaveTo="opacity-0"
						>
							<Dialog.Overlay className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
						</Transition.Child>

						{/* This element is to trick the browser into centering the modal contents. */}
						<span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
							&#8203;
						</span>
						<Transition.Child
							as={Fragment}
							enter="ease-out duration-300"
							enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
							enterTo="opacity-100 translate-y-0 sm:scale-100"
							leave="ease-in duration-200"
							leaveFrom="opacity-100 translate-y-0 sm:scale-100"
							leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
						>
							<div className="inline-block px-4 pt-5 pb-4 overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full sm:p-6">
							<div className="sm:flex sm:items-start">
								<div className="flex items-center justify-center flex-shrink-0 w-12 h-12 mx-auto bg-red-100 rounded-full sm:mx-0 sm:h-10 sm:w-10">
								<ExclamationIcon className="w-6 h-6 text-red-600" aria-hidden="true" />
								</div>
								<div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
								<Dialog.Title as="h3" className="text-lg font-medium leading-6 text-gray-900">
									Delete template
								</Dialog.Title>
								<div className="mt-2">
									<p className="text-sm text-gray-500">
									Are you sure you want to delete template? All of your data will be permanently removed
									from our servers forever. This action can be undone.
									</p>
								</div>
								</div>
							</div>
							<div className="mt-5 sm:mt-4 sm:flex sm:flex-row-reverse">
								<button
								type="button"
								className="inline-flex justify-center w-full px-4 py-2 text-base font-medium text-white bg-red-600 border border-transparent rounded-md shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm"
								onClick={deleteTemplate}
								>
								Delete
								</button>
								<button
								type="button"
								className="inline-flex justify-center w-full px-4 py-2 mt-3 text-base font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:w-auto sm:text-sm"
								onClick={() => setOpen(false)}
								ref={cancelButtonRef}
								>
								Cancel
								</button>
							</div>
							</div>
						</Transition.Child>
						</div>
					</Dialog>
					</Transition.Root>
				}

				{
					showTemplate &&
					<div className="tw-form-section-wrapper">
						{ReactHtmlParser(addData)}
					</div>
				}

            </div>


        );

}

if (document.getElementById('email-template')) {
    render(<ToastProvider autoDismiss="true"><Email /></ToastProvider>, document.getElementById('email-template'))
}
