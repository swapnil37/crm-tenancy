import React, {useState, useEffect} from "react"
import {useForm} from "react-hook-form"
import {render} from "react-dom"
import { Switch } from '@headlessui/react'


import axios from "axios";
import {ToastProvider, useToasts} from 'react-toast-notifications'

import ErrorSVG from "../common/ErrorSVG"

function classNames(...classes) {
	return classes.filter(Boolean).join(' ')
}

function Setting() {
	const {addToast} = useToasts();
	const [maintenance_enabled, setMaintenanceEnabled] = useState(false);
	const [domain_enabled, setDomainEnabled] = useState(false);
	const [payments_enabled, setPaymentsEnabled] = useState(false);
	const [plan_enabled, setPlanEnabled] = useState(false);
	const [registration_enabled, setRegistrationEnabled] = useState(false);

	const {register, handleSubmit, reset, formState: {errors}} = useForm()

	useEffect(() => {
		const fetchData = async () => {
			const result = await axios('/user/settings')
			reset(result.data)
		};

		fetchData()
	}, [reset])

	const onSubmit = (data) => {
		axios.patch('/user/settings/update', {
			maintenance: maintenance_enabled,
		}).then((response) => {
			addToast('Saved Successfully', {appearance: 'success'});
		}).catch(function (error) {
			addToast(error.message, {appearance: 'error'});
			console.log(error)
		});
	}

	const handleCancel = () => {
		reset()
		addToast('All changes reverted successfully.', {appearance: 'success'});
	}

	return (
			<div>
				<div className="pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
					<h3 className="text-lg font-medium leading-6 text-gray-900">
						Notification
					</h3>
				</div>


				{/* Maitenance Settings */}

				<form onSubmit={handleSubmit()}>
					<div className="mt-4 space-y-6">
						<div>
							<div className="tw-form-section-wrapper">
								<div className="md:grid md:grid-cols-3 md:gap-6">
									<div className="mt-5 md:mt-0 md:col-span-3">
										<div className="space-y-6">

											{/* Maintenance  */}
											<div className="flex justify-between">
												<div>
													<label className="tw-form-label">Site put on maintenance</label>
												</div>
												<div>
													<Switch
														checked={maintenance_enabled}
														onChange={setMaintenanceEnabled}
														className={classNames(
															maintenance_enabled ? 'bg-indigo-600' : 'bg-gray-200',
															'tw-btn-switch'
														)}
														>
														<span className="sr-only">Use setting</span>
														<span
															aria-hidden="true"
															className={classNames(
															maintenance_enabled ? 'translate-x-5' : 'translate-x-0',
															'tw-btn-switch-circle'
															)}
														/>
													</Switch>
												</div>
											</div>

											{/* Domain */}
											<div className="flex justify-between">
												<div>
													<label className="tw-form-label">Domain Add & Delete</label>
												</div>
												<div>
													<Switch
														checked={domain_enabled}
														onChange={setDomainEnabled}
														className={classNames(
															domain_enabled ? 'bg-indigo-600' : 'bg-gray-200',
															'tw-btn-switch'
														)}
														>
														<span className="sr-only">Use setting</span>
														<span
															aria-hidden="true"
															className={classNames(
															domain_enabled ? 'translate-x-5' : 'translate-x-0',
															'tw-btn-switch-circle'
															)}
														/>
													</Switch>
												</div>
											</div>

											{/* Payments */}
											<div className="flex justify-between">
												<div>
													<label className="tw-form-label">Failed & Pending payments</label>
												</div>
												<div>
													<Switch
														checked={payments_enabled}
														onChange={setPaymentsEnabled}
														className={classNames(
															payments_enabled ? 'bg-indigo-600' : 'bg-gray-200',
															'tw-btn-switch'
														)}
														>
														<span className="sr-only">Use setting</span>
														<span
															aria-hidden="true"
															className={classNames(
															payments_enabled ? 'translate-x-5' : 'translate-x-0',
															'tw-btn-switch-circle'
															)}
														/>
													</Switch>
												</div>
											</div>

											{/* Plans */}
											<div className="flex justify-between">
												<div>
													<label className="tw-form-label">Plan Expiry</label>
												</div>
												<div>
													<Switch
														checked={plan_enabled}
														onChange={setPlanEnabled}
														className={classNames(
															plan_enabled ? 'bg-indigo-600' : 'bg-gray-200',
															'tw-btn-switch'
														)}
														>
														<span className="sr-only">Use setting</span>
														<span
															aria-hidden="true"
															className={classNames(
															plan_enabled ? 'translate-x-5' : 'translate-x-0',
															'tw-btn-switch-circle'
															)}
														/>
													</Switch>
												</div>
											</div>

											{/* User Registration */}
											<div className="flex justify-between">
												<div>
													<label className="tw-form-label">New Registration</label>
												</div>
												<div>
													<Switch
														checked={registration_enabled}
														onChange={setRegistrationEnabled}
														className={classNames(
															registration_enabled ? 'bg-indigo-600' : 'bg-gray-200',
															'tw-btn-switch'
														)}
														>
														<span className="sr-only">Use setting</span>
														<span
															aria-hidden="true"
															className={classNames(
															registration_enabled ? 'translate-x-5' : 'translate-x-0',
															'tw-btn-switch-circle'
															)}
														/>
													</Switch>
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>
							<div className="tw-form-button-wrapper">
								<button type="button" className="tw-btn tw-btn-white" onClick={handleCancel}>Cancel</button>
								<button type="submit" className="tw-btn tw-btn-indigo">Save</button>
							</div>
						</div>
					</div>
				</form>

			</div>
	)
}

if (document.getElementById('notification')) {
	render(<ToastProvider autoDismiss="true"><Setting /></ToastProvider>, document.getElementById('notification'))
}
