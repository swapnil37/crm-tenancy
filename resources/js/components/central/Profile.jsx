import React, {useState, useEffect} from "react"
import {useForm} from "react-hook-form"
import {render} from "react-dom"

import axios from "axios";
import {ToastProvider, useToasts} from 'react-toast-notifications'

import ErrorSVG from "../common/ErrorSVG"

function Profile() {
	const {addToast} = useToasts();
	const {register, handleSubmit, reset, formState: {errors}} = useForm()

	useEffect(() => {
		const fetchData = async () => {
			const result = await axios('/user/profile')
			reset(result.data)
		};

		fetchData()
	}, [reset])

	const onSubmit = (data) => {
		axios.patch('/user/profile/update', {
			first_name: data.first_name,
			last_name: data.last_name,
			email: data.email,
			phone: data.phone,
		}).then((response) => {
			addToast('Saved Successfully', {appearance: 'success'});
		}).catch(function (error) {
			addToast(error.message, {appearance: 'error'});
			console.log(error)
		});
	}

	const handleCancel = () => {
		reset()
		addToast('All changes reverted successfully.', {appearance: 'success'});
	}

	return (
			<div>
				<div className="pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
					<h3 className="text-lg leading-6 font-medium text-gray-900">
						My Profile
					</h3>
				</div>

				<form onSubmit={handleSubmit(onSubmit)}>
					<div className="mt-2 space-y-6">
						<div>
							<div className="tw-form-section-wrapper">
								<div className="md:grid md:grid-cols-3 md:gap-6">
									<div className="md:col-span-1">
										<h3 className="tw-form-section-heading">Personal Details</h3>
									</div>
									<div className="mt-5 md:mt-0 md:col-span-2">
										<div className="space-y-6">
											<div>
												<label htmlFor="first_name" className="tw-form-label">First Name</label>
												<div className="relative">
													<div className="mt-1 flex rounded-md shadow-sm relative">
														<input {...register("first_name", {required: true})}
														       className="tw-form-input"
														/>
														{errors.first_name && <ErrorSVG />}
													</div>
													{errors.first_name && <p className="tw-form-input-error">Invalid name.</p>}
												</div>
											</div>

											<div>
												<label htmlFor="last_name" className="tw-form-label">Last Name</label>
												<div className="relative">
													<div className="mt-1 flex rounded-md shadow-sm relative">
														<input {...register("last_name", {required: true})}
														       className="tw-form-input"
														/>
														{errors.last_name && <ErrorSVG />}
													</div>
													{errors.last_name && <p className="tw-form-input-error">Invalid name.</p>}
												</div>
											</div>

											<div>
												<label htmlFor="email" className="tw-form-label">Email</label>
												<div className="relative">
													<div className="mt-1 flex rounded-md shadow-sm relative">
														<input {...register("email", {required: true})}
														       className="tw-form-input"
														/>
														{errors.email && <ErrorSVG />}
													</div>
													{errors.email && <p className="tw-form-input-error">Invalid email.</p>}
												</div>
											</div>

											<div>
												<label htmlFor="phone" className="tw-form-label">Phone</label>
												<div className="relative">
													<div className="mt-1 flex rounded-md shadow-sm relative">
														<input {...register("phone", {required: true})}
														       className="tw-form-input"
														/>
														{errors.phone && <ErrorSVG />}
													</div>
													{errors.phone && <p className="tw-form-input-error">Invalid phone.</p>}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="tw-form-button-wrapper">
								<button type="button" className="tw-btn tw-btn-white" onClick={handleCancel}>Cancel</button>
								<button type="submit" className="tw-btn tw-btn-indigo">Save</button>
							</div>
						</div>
					</div>
				</form>

			</div>
	)
}

if (document.getElementById('profile')) {
	render(<ToastProvider autoDismiss="true"><Profile /></ToastProvider>, document.getElementById('profile'))
}
