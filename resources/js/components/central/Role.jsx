import React, {useState, useEffect} from "react"
import {useForm} from "react-hook-form"
import {render} from "react-dom"

import axios from "axios";
import ReactDataGrid from '@inovua/reactdatagrid-enterprise'
import '@inovua/reactdatagrid-enterprise/index.css'
import { ToastProvider, useToasts } from 'react-toast-notifications'

import ErrorSVG from "../common/ErrorSVG"

function Role(){
    const { addToast } = useToasts();
    const [showRoleForm, setShowRoleForm] = useState(false)
    const [showPermissionForm, setShowPermissionForm] = useState(false)
    const [roles, setRoles] = useState([])
    const [permissions, setPermissions] = useState([])
    const {register, handleSubmit, formState: {errors}} = useForm()
    const gridStyle = { minHeight: 400 }

    const roleColumns = [
        {
            name: 'id',
            header: 'ID',
            defaultWidth: 80,
            defaultVisible: false
        },
        {
            name: 'name',
            header: 'Role Name',
            minWidth: 50,
            defaultFlex: 1
        },
        {
            name: 'permissions',
            header: 'Assigned Permissions',
            maxWidth: 1000,
            defaultFlex: 1,
            render: ({ value }) => value === undefined ? null : (value.map((permission, id) => <span key={id} className="tw-marker bg-yellow-100 text-yellow-800 mr-2">{permission.name}</span>))
        },
        {
            name: 'updated_at',
            header: 'Last Modified',
            maxWidth: 1000,
            defaultFlex: 1,
            render: ({ value }) => value === undefined ? null : (new Date(value)).toDateString() + ' ' +(new Date(value)).toLocaleTimeString()
        }
    ]

    const permissionColumns = [
        {
            name: 'id',
            header: 'ID',
            defaultWidth: 80,
            defaultVisible: false
        },
        {
            name: 'name',
            header: 'Permission Type',
            minWidth: 50,
            defaultFlex: 1
        },
        {
            name: 'guard_name',
            header: 'Guard',
            maxWidth: 1000,
            defaultFlex: 1
        },
        {
            name: 'updated_at',
            header: 'Last Modified',
            maxWidth: 1000,
            defaultFlex: 1,
            render: ({ value }) => value === undefined ? null : (new Date(value)).toDateString() + ' ' +(new Date(value)).toLocaleTimeString()
        }
    ]

    useEffect(() => {
        const fetchRoleData = async () => {
            const result = await axios(
                '/user/roles',
            )

            setRoles(result.data)
        };

        const fetchPermissionData = async () => {
            const result = await axios(
                '/user/permissions',
            )

            setPermissions(result.data)
        };

        fetchRoleData()
        fetchPermissionData()
    }, [])

    const onRoleSubmit = (data) => {
        axios.post('/user/roles', {
            name: data.role
        }).then((response) => {
            setRoles([...roles, response.data])
            addToast('Saved Successfully', { appearance: 'success' });
            setShowRoleForm(false)
        }).catch(function (error) {
            addToast(error.message, { appearance: 'error' });
            console.log(error)
        });
    }

    const onPermissionSubmit = (data) => {
        axios.post('/user/permissions', {
            name: data.permission,
            roles: data.roles
        }).then((response) => {
            setPermissions([...permissions, response.data])

            setShowPermissionForm(false)
        }).catch(function (error) {
            console.log(error)
        });
    }

    return (
        <div>

            {/*---------------------------
             |          ROLES
             |---------------------------*/}

            <div className="pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
                <h3 className="text-lg leading-6 font-medium text-gray-900">
                    Roles
                </h3>
                <div className="mt-3 flex sm:mt-0 sm:ml-4 space-x-3">
                    <button type="button"
                            className="tw-btn tw-btn-green"
                            onClick={() => setShowRoleForm(true)}
                    >
                        Create Role
                    </button>
                </div>
            </div>

            {
                !showRoleForm &&
                <ReactDataGrid
                    idProperty="id"
                    columns={roleColumns}
                    dataSource={roles}
                    style={gridStyle}
                />
            }

            {
                showRoleForm &&
                <form onSubmit={handleSubmit(onRoleSubmit)}>
                    <div className="mt-2 space-y-6">
                        <div>
                            <div className="tw-form-section-wrapper">
                                <div className="md:grid md:grid-cols-3 md:gap-6">
                                    <div className="md:col-span-1">
                                        <h3 className="tw-form-section-heading">User Role</h3>
                                        <p className="tw-form-section-subheading">
                                            Role name must be unique.
                                        </p>
                                    </div>

                                    <div className="mt-5 md:mt-0 md:col-span-2">
                                        <div className="space-y-6">
                                            <div className="grid grid-cols-3 gap-6">
                                                <div className="col-span-3 sm:col-span-2">
                                                    <label htmlFor="role" className="tw-form-label">
                                                        Role Name
                                                    </label>
                                                    <div className="mt-1 flex rounded-md shadow-sm relative">
                                                        <input {...register("role", {required: true})}
                                                               className="tw-form-input mt-0 rounded-l-none rounded-r-none"
                                                               placeholder="e.g. Admin"
                                                        />
                                                        {errors.role && <Error />}
                                                    </div>
                                                    {errors.role && <p className="tw-form-input-error">Invalid role name.</p>}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="tw-form-button-wrapper">
                                <button type="button"
                                        className="tw-btn tw-btn-white"
                                        onClick={() => setShowRoleForm(false)}
                                >
                                    Cancel
                                </button>

                                <button type="submit" className="tw-btn tw-btn-indigo">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            }




            {/*---------------------------
             |          PERMISSIONS
             |---------------------------*/}

            <div className="mt-10 pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
                <h3 className="text-lg leading-6 font-medium text-gray-900">
                    Permissions
                </h3>
                <div className="mt-3 flex sm:mt-0 sm:ml-4 space-x-3">
                    <button type="button"
                            className="tw-btn tw-btn-indigo"
                            onClick={() => setShowPermissionForm(true)}
                    >
                        Create Permission
                    </button>
                </div>
            </div>

            {
                !showPermissionForm &&
                <ReactDataGrid
                    idProperty="id"
                    columns={permissionColumns}
                    dataSource={permissions}
                    style={gridStyle}
                />
            }

            {
                showPermissionForm &&
                <form onSubmit={handleSubmit(onPermissionSubmit)}>
                    <div className="mt-2 space-y-6">
                        <div>
                            <div className="tw-form-section-wrapper">
                                <div className="md:grid md:grid-cols-3 md:gap-6">
                                    <div className="md:col-span-1">
                                        <h3 className="tw-form-section-heading">User Permissions</h3>
                                        <p className="tw-form-section-subheading">
                                            Permission name must be unique.
                                        </p>
                                    </div>

                                    <div className="mt-5 md:mt-0 md:col-span-2">
                                        <div className="space-y-6">
                                            <div>
                                                <label htmlFor="role" className="tw-form-label">
                                                    Permission Name
                                                </label>
                                                <div className="relative">
                                                    <div className="mt-1 flex rounded-md shadow-sm relative">
                                                        <input {...register("permission", {required: true})}
                                                               className="tw-form-input"
                                                               placeholder="e.g. create user"
                                                        />
                                                        {errors.permission && <ErrorSVG />}
                                                    </div>
                                                    {errors.permission && <p className="tw-form-input-error">Invalid permission name.</p>}
                                                </div>
                                            </div>

                                            <div>
                                                <label htmlFor="role" className="tw-form-label">
                                                    Select Roles
                                                </label>
                                                <div className="relative">
                                                    <div className="mt-1 flex rounded-md shadow-sm relative">
                                                        <select {...register("roles")}
                                                                multiple
                                                                className="tw-form-select"
                                                        >
                                                            {roles.map((roleName, index) => (
                                                                <option value={roleName.name}
                                                                        key={index}
                                                                >
                                                                    {roleName.name}
                                                                </option>
                                                            ))}
                                                        </select>
                                                        {errors.roles && <ErrorSVG />}
                                                    </div>
                                                    {errors.roles && <p className="tw-form-input-error">Invalid role name.</p>}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="tw-form-button-wrapper">
                                <button type="button"
                                        className="tw-btn tw-btn-white"
                                        onClick={() => setShowPermissionForm(false)}
                                >
                                    Cancel
                                </button>

                                <button type="submit" className="tw-btn tw-btn-indigo">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            }

        </div>
    )
}

if (document.getElementById('role')) {
    render(<ToastProvider autoDismiss="true"><Role /></ToastProvider>, document.getElementById('role'))
}
