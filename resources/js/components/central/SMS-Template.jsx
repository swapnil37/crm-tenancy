import React, { Component, useState, useEffect } from 'react';
import {useForm} from "react-hook-form"
import axios from "axios";
import ReactDataGrid from '@inovua/reactdatagrid-enterprise'
import '@inovua/reactdatagrid-enterprise/index.css'
import { ToastProvider, useToasts } from 'react-toast-notifications'
import { PencilAltIcon, TrashIcon, EyeIcon } from '@heroicons/react/outline'
//import SimpleUploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/simpleuploadadapter';



import {render} from "react-dom";



function SMS(){
	const { addToast } = useToasts();
	const [showTemplateForm, setShowTemplateForm] = useState(false)
	const [Editor, setEditor] = useState('')
	const [templates, setTepmlates] = useState([])
	const {register, handleSubmit, setValue, reset, formState: {errors}} = useForm()
	const gridStyle = { minHeight: 700 }
    const columns = [
        {
        	name: 'name',
	        header: 'Template Name',
	        minWidth: 300,
	        defaultFlex: 1,
	        render: ({value}) => value
        },
	    {
        	name: 'template',
	        header: 'Template',
	        minWidth: 650,
	        defaultFlex: 1,
		    render: ({ value }) => value
        },
		{
        	name: 'id',
	        header: 'Action',
	        defaultWidth: 80,
	        defaultFlex: 1,
            render: ({ value }) => <div className="grid grid-cols-5 space-x-1"><div><EyeIcon className="w-4 h-4 text-indigo-700 cursor-pointer" onClick={editTemplate(value)}/></div> <div><PencilAltIcon className="w-4 h-4 text-indigo-700 cursor-pointer" onClick={editTemplate(value)} /></div> <div><TrashIcon className="w-4 h-4 text-red-700 cursor-pointer" onClick={destroyTemplate(value)} /></div></div>
        }

    ]

	function onEditorChange(evt, editor) {
		setEditor(
			editor.getData()
		)
    }

	const onSubmit = (data) => {
		const template = document.getElementById('')
        axios.post('/user/sms-template', {
            name: data.name,
            template: data.template,
        }).then((response) => {
	        addToast('Saved Successfully', { appearance: 'success' });
            setTepmlates([...templates, response.data])
            setShowTemplateForm(false)
        }).catch(function (error) {
            addToast(error.message, { appearance: 'error' });
            console.log(error)
        });
    }

	const editTemplate = value => () => {
		axios.get('/user/sms-template/edit/'+value, {
        }).then((response) => {
			const values = response.data
			console.log(values);
            setShowTemplateForm(true)
			setValue('name', values.name);
			setValue('template', values.template);
			//CKEditor.replace( '<p>This is a new paragraph.</p>' );

        }).catch(function (error) {
            addToast(error.message, { appearance: 'error' });
            console.log(error)
        });
	}

	const destroyTemplate = value => () => {
		axios.post('/user/sms-template/delete/', {
			id: value
        }).then((response) => {
			if(response.data==1){
				addToast('Deleted successfully!', { appearance: 'success' });
				const fetchData = async () => {
					const result = await axios(
						'/user/sms-template',
					)
					setTepmlates(result.data)
				};
				fetchData()
			}
        }).catch(function (error) {
            addToast(error.message, { appearance: 'error' });
            console.log(error)
        });
	};

	useEffect(() => {
        const fetchData = async () => {
            const result = await axios(
                '/user/sms-template',
            )

			setTepmlates(result.data)
        };

        fetchData()
    }, [])

        return (
            <div className="App">
				<div className="pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
					<h3 className="text-lg font-medium leading-6 text-gray-900">
						SMS Templates
					</h3>
					<div className="flex mt-3 space-x-3 sm:mt-0 sm:ml-4">
						<button type="button"
								className="tw-btn tw-btn-green"
								onClick={() => {setShowTemplateForm(true); reset();}}
						>
							Create
						</button>
						<button type="button" className="tw-btn tw-btn-indigo" onClick={() => setShowTemplateForm(false)}>View</button>
					</div>
				</div>

				{
					!showTemplateForm &&
					<ReactDataGrid
						idProperty="id"
						columns={columns}
						dataSource={templates}
						style={gridStyle}
					/>
				}
				{
				showTemplateForm &&
				<form onSubmit={handleSubmit(onSubmit)}>
					<div className="tw-form-section-wrapper">
						<div className="mt-5 mb-8 space-y-6 md:mt-0">
							<div className="grid grid-cols-2 gap-6">

								<div>
									<label>Template Name</label>
									<input {...register("name", {required: true})}
										className="mt-0 tw-form-input"
										placeholder="e.g. Thanks for Signup"
									/>
								</div>

								<div>
									<label>Variables</label>
									<select
										className="mt-0 rounded-md shadow-sm tw-form-select"
									>
										<option value="name">Name</option>
										<option value="email">Email</option>
										<option value="password">Password</option>
										<option value="org_name">Organization Name</option>
									</select>
								</div>
							</div>
							<div>
								<label>Message</label>
								<textarea {...register("template", {required: true})}
									className="h-48 mt-0 tw-form-input"
								/>
							</div>
						</div>



					</div>
					<div className="tw-form-button-wrapper">
						<button type="button"
								className="tw-btn tw-btn-white"
								onClick={() => setShowTemplateForm(false)}
						>
							Cancel
						</button>
						<button type="submit" className="tw-btn tw-btn-indigo">Save</button>
					</div>
				</form>

				}

            </div>


        );

}

if (document.getElementById('sms-template')) {
    render(<ToastProvider autoDismiss="true"><SMS /></ToastProvider>, document.getElementById('sms-template'))
}
