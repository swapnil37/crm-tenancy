import React, {useState, useEffect} from "react"
import {useForm} from "react-hook-form"
import {render} from "react-dom"
import { Switch } from '@headlessui/react'

import axios from "axios";
import {ToastProvider, useToasts} from 'react-toast-notifications'

import ErrorSVG from "../common/ErrorSVG"

function classNames(...classes) {
	return classes.filter(Boolean).join(' ')
}

function Setting() {
	const {addToast} = useToasts();
	const [maintenance_enabled, setMaintenanceEnabled] = useState(false);
	const [debugging_enabled, setDebuggingEnabled] = useState(false);
	const {register, handleSubmit, reset, formState: {errors}} = useForm();
	const {register:register2, handleSubmit:handleSubmit2, formState: {errors:errors2}} = useForm();


	useEffect(() => {
		const fetchData = async () => {
			const result = await axios('/user/settings')
			reset(result.data)
		};

		fetchData()
	}, [reset])



	const onSubmit = (data) => {
		axios.patch('/user/settings/update', {
			site_name: data.site_name,
			site_active: data.site_active,
			email_api: data.emailapi,
			email_api_key: data.email_api_key,
			email_secret_key: data.email_secret_key,
			sms_api_key : data.sms_api_key,
			payment_api_key: data.payment_api_key,
			storage_type: data.storagetype,
		}).then((response) => {
			addToast('Saved Successfully', {appearance: 'success'});
		}).catch(function (error) {
			addToast(error.message, {appearance: 'error'});
			console.log(error)
		});
	}


	const handleCancel = () => {
		reset()
		md_reset()
		addToast('All changes reverted successfully.', {appearance: 'success'});
	}

	const handleChange = (selector, event) => {
		if (selector === "emailapi") {

		} else {
		  // Other logic
		}

	}

	return (
			<div>
				<div className="pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
					<h3 className="text-lg font-medium leading-6 text-gray-900">
						Global Settings
					</h3>
				</div>

				<form onSubmit={handleSubmit(onSubmit)}>
					<div className="mt-2 space-y-6">
						<div className="space-y-6 tw-form-section-wrapper">

							{/* General Settings */}

							<div className="md:grid md:grid-cols-3 md:gap-6">
								<div className="md:col-span-1">
									<h3 className="tw-form-section-heading">General Settings</h3>
								</div>
								<div className="mt-5 space-y-6 md:mt-0 md:col-span-2">

									<div>
										<label htmlFor="site_name" className="tw-form-label">Site Name</label>
										<div className="relative">
											<div className="relative flex mt-1 rounded-md shadow-sm">
												<input {...register("site_name", {required: true})}
														className="tw-form-input"
												/>
												{errors.site_name && <ErrorSVG />}
											</div>
											{errors.site_name && <p className="tw-form-input-error">Invalid name.</p>}
										</div>
									</div>

									<div>
										<label htmlFor="site_active" className="tw-form-label">Site Name</label>
										<div className="relative">
											<div className="relative flex mt-1 rounded-md shadow-sm">
												<input {...register("site_active", {required: true})}
														className="tw-form-input"
												/>
												{errors.site_active && <ErrorSVG />}
											</div>
											{errors.site_active && <p className="tw-form-input-error">Invalid name.</p>}
										</div>
									</div>

								</div>
							</div>

							{/* Email Settings */}

							<div className="md:grid md:grid-cols-3 md:gap-6">
								<div className="md:col-span-1">
									<h3 className="tw-form-section-heading">Email Settings</h3>
								</div>
								<div className="mt-5 space-y-6 md:mt-0 md:col-span-2">

									<div>
										<label htmlFor="emailapi" className="tw-form-label">Default Email Service</label>
										<div className="relative">
											<div className="relative flex mt-1 rounded-md shadow-sm">
												<select {...register("emailapi", { required: true })}
												 className="tw-form-select"
												>
													<option value="sendgrid">Sendgrid</option>
													<option value="amazon">Amazon SES</option>
												</select>
											</div>
										</div>
									</div>

									<div>
										<label htmlFor="email_api_key" className="tw-form-label">API Key</label>
										<div className="relative">
											<div className="relative flex mt-1 rounded-md shadow-sm">
												<input {...register("email_api_key", {required: true})}
														className="tw-form-input"
												/>
												{errors.email_api_key && <ErrorSVG />}
											</div>
											{errors.email_api_key && <p className="tw-form-input-error">Invalid Key.</p>}
										</div>
									</div>

									<div>
										<label htmlFor="email_secret_key" className="tw-form-label">Secret Key</label>
										<div className="relative">
											<div className="relative flex mt-1 rounded-md shadow-sm">
												<input {...register("email_secret_key")}
													className="tw-form-input"
												/>
												{errors.email_secret_key && <ErrorSVG />}
											</div>
											{errors.email_secret_key && <p className="tw-form-input-error">Invalid Key.</p>}
										</div>
									</div>

								</div>
								<div className="md:col-span-1">
									<button type="button" className="tw-btn tw-btn-white">Test Email Setting</button>
								</div>
							</div>

							{/* SMS Settings */}

							<div className="md:grid md:grid-cols-3 md:gap-6">
								<div className="md:col-span-1">
									<h3 className="tw-form-section-heading">SMS Settings</h3>
								</div>
								<div className="mt-5 space-y-6 md:mt-0 md:col-span-2">

									<div>
										<label htmlFor="sms_api_key" className="tw-form-label">API Key</label>
										<div className="relative">
											<div className="relative flex mt-1 rounded-md shadow-sm">
												<input {...register("sms_api_key", {required: true})}
													className="tw-form-input"
												/>
												{errors.sms_api_key && <ErrorSVG />}
											</div>
											{errors.sms_api_key && <p className="tw-form-input-error">Invalid Key.</p>}
										</div>
									</div>

								</div>
								<div className="md:col-span-1">
									<button type="button" className="tw-btn tw-btn-white">Test SMS Setting</button>
								</div>
							</div>

							{/* Payment Gateways */}

							<div className="md:grid md:grid-cols-3 md:gap-6">
								<div className="md:col-span-1">
									<h3 className="tw-form-section-heading">Payment Gateway</h3>
								</div>
								<div className="mt-5 space-y-6 md:mt-0 md:col-span-2">

									<div>
										<label htmlFor="payment_api_key" className="tw-form-label">API Key</label>
										<div className="relative">
											<div className="relative flex mt-1 rounded-md shadow-sm">
												<input {...register("payment_api_key", {required: true})}
													className="tw-form-input"
												/>
												{errors.payment_api_key && <ErrorSVG />}
											</div>
											{errors.payment_api_key && <p className="tw-form-input-error">Invalid Key.</p>}
										</div>
									</div>

								</div>
							</div>

							{/* Storage Settings */}

							<div className="md:grid md:grid-cols-3 md:gap-6">
								<div className="md:col-span-1">
									<h3 className="tw-form-section-heading">Storage Settings</h3>
								</div>
								<div className="mt-5 space-y-6 md:mt-0 md:col-span-2">

									<div>
										<label htmlFor="storagetype" className="tw-form-label">Default Storage Type</label>
										<div className="relative">
											<div className="relative flex mt-1 rounded-md shadow-sm">
												<select {...register("storagetype", { required: true })}
													className="tw-form-select"
												>
													<option value="local">Local</option>
													<option value="aws">AWS</option>
												</select>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
					<div className="tw-form-button-wrapper">
						<button type="button" className="tw-btn tw-btn-white" onClick={handleCancel}>Cancel</button>
						<button type="submit" className="tw-btn tw-btn-indigo">Save</button>
					</div>
				</form>

				{/* Maitenance Settings */}

				<form onSubmit={handleSubmit()}>
					<div className="mt-4 tw-form-section-wrapper">
						<div className="md:grid md:grid-cols-3 md:gap-6">
							<div className="md:col-span-1">
								<h3 className="tw-form-section-heading">Maitenance & Debugging</h3>
							</div>
							<div className="mt-5 space-y-6 md:mt-0 md:col-span-2">

								<div className="flex justify-between">
									<div>
										<label className="tw-form-label">Enable Debugging</label>
									</div>
									<div>
										<Switch
											checked={debugging_enabled}
											onChange={setDebuggingEnabled}
											className={classNames(
												debugging_enabled ? 'bg-indigo-600' : 'bg-gray-200',
												'tw-btn-switch'
											)}
											>
											<span className="sr-only">Use setting</span>
											<span
												aria-hidden="true"
												className={classNames(
												debugging_enabled ? 'translate-x-5' : 'translate-x-0',
												'tw-btn-switch-circle'
												)}
											/>
										</Switch>
									</div>
								</div>

								<div className="flex justify-between">
									<div>
										<label className="tw-form-label">Put site on maintenance</label>
									</div>
									<div>
										<Switch
											checked={maintenance_enabled}
											onChange={setMaintenanceEnabled}
											className={classNames(
												maintenance_enabled ? 'bg-indigo-600' : 'bg-gray-200',
												'tw-btn-switch'
											)}
											>
											<span className="sr-only">Use setting</span>
											<span
												aria-hidden="true"
												className={classNames(
													maintenance_enabled ? 'translate-x-5' : 'translate-x-0',
												'tw-btn-switch-circle'
												)}
											/>
										</Switch>
									</div>
								</div>

								<div>
									<label htmlFor="sentry_api_key" className="tw-form-label">Sentry API Key</label>
									<div className="relative">
										<div className="relative flex mt-1 rounded-md shadow-sm">
											<input {...("sentry_api_key", {required: true})}
													className="tw-form-input"
											/>
											{errors.sentry_api_key && <ErrorSVG />}
										</div>
										{errors.sentry_api_key && <p className="tw-form-input-error">Invalid Key.</p>}
									</div>
								</div>

							</div>
						</div>
					</div>

					<div className="tw-form-button-wrapper">
						<button type="button" className="tw-btn tw-btn-white" onClick={handleCancel}>Cancel</button>
						<button type="submit" className="tw-btn tw-btn-indigo">Save</button>
					</div>

				</form>

			</div>
	)
}

if (document.getElementById('setting')) {
	render(<ToastProvider autoDismiss="true"><Setting /></ToastProvider>, document.getElementById('setting'))
}
