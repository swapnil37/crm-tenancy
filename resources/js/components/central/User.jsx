import React, {useState, useEffect} from "react"
import {useForm} from "react-hook-form"
import Select from "react-select";
import {render} from "react-dom"

import axios from "axios";
import ReactDataGrid from '@inovua/reactdatagrid-enterprise'
import '@inovua/reactdatagrid-enterprise/index.css'
import { PencilAltIcon, TrashIcon } from '@heroicons/react/outline'
import { ToastProvider, useToasts } from 'react-toast-notifications'

import ErrorSVG from "../common/ErrorSVG"

function User(){
    const { addToast } = useToasts();
    const [showForm, setShowForm] = useState(false)
    const [domains, setDomains] = useState([])
    const {register, handleSubmit, reset, formState: {errors}} = useForm()
	const [roles, setRoles] =  useState([])
    const gridStyle = { minHeight: 700 }
    const columns = [
	    {
		    name: 'id',
		    header: 'ID',
		    maxWidth: 1000,
		    defaultFlex: 1,
		    defaultVisible: false
	    },
        {
        	name: 'first_name',
	        header: 'Name',
	        minWidth: 50,
	        defaultFlex: 1,
	        render: ({value}) => value + '.edutra.io'
        },
	    {
        	name: 'organisation',
	        header: 'Organisation',
	        minWidth: 50,
	        defaultFlex: 1,
		    render: ({ data }) => data.tenant.organisation
        },
	    {
		    name: 'email',
		    header: 'Admin Email',
		    maxWidth: 1000,
		    defaultFlex: 1,
		    render: ({ data }) => data.tenant.email
	    },
	    {
		    name: 'contact',
		    header: 'Contact Person',
		    maxWidth: 1000,
		    defaultFlex: 1,
		    defaultVisible: false,
		    render: ({ data }) => data.tenant.first_name + ' ' + data.tenant.last_name
	    },
	    {
		    name: 'phone',
		    header: 'Phone Number',
		    maxWidth: 1000,
		    defaultFlex: 1,
		    defaultVisible: false,
		    render: ({ data }) => data.tenant.phone
	    },
        {
            name: 'created_at',
            header: 'Created',
            maxWidth: 1000,
            defaultFlex: 1,
            render: ({ value }) => (new Date(value)).toDateString() + ' ' +(new Date(value)).toLocaleTimeString()
        },
		{
        	name: 'id',
	        header: 'Action',
	        defaultWidth: 80,
	        defaultFlex: 1,
            render: ({ value }) => <div className="grid grid-cols-8 space-x-1"><div><PencilAltIcon className="w-4 h-4 text-indigo-700" /></div> <div><TrashIcon className="w-4 h-4 text-red-700" /></div></div>
        }

    ];

	const options = [
		{ value: 'blues', label: 'Blues' },
		{ value: 'rock', label: 'Rock' },
		{ value: 'jazz', label: 'Jazz' },
		{ value: 'orchestra', label: 'Orchestra' }
	];

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios(
                '/user/user',
            )

            setDomains(result.data)
        };

		const fetchRoleData = async () => {
            const roles = await axios(
                '/user/roles',
            )

			reset(roles.data);
			setRoles(roles.data);
        };

		fetchRoleData()
        fetchData()
    }, [])

    const onSubmit = (data) => {
        axios.post('/user/domains', {
            domain: data.domain,
            organisation: data.organisation,
            first_name: data.first_name,
            last_name: data.last_name,
            email: data.email,
	        phone: data.phone,
        }).then((response) => {
	        addToast('Saved Successfully', { appearance: 'success' });
            setDomains([...domains, response.data])
            setShowForm(false)
        }).catch(function (error) {
            addToast(error.message, { appearance: 'error' });
            console.log(error)
        });
    }



    return (
        <div>
            <div className="pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
                <h3 className="text-lg font-medium leading-6 text-gray-900">
                    Users
                </h3>
                <div className="flex mt-3 space-x-3 sm:mt-0 sm:ml-4">
                    <button type="button" className="tw-btn tw-btn-green" onClick={() => setShowForm(true)}>Create</button>
                    <button type="button" className="tw-btn tw-btn-indigo" onClick={() => setShowForm(false)}>View</button>
                </div>
            </div>

	        <div className="p-4 mb-4 rounded-md bg-cyan-600">
		        <div className="flex">
			        <div className="flex-shrink-0">
				        <svg className="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
					        <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clipRule="evenodd" />
				        </svg>
			        </div>
			        <div className="ml-3 text-sm font-medium text-white">
				        Full contact detail can be viewed on demand by clicking on the header row and selecting Columns.
			        </div>
		        </div>
	        </div>

            {
                !showForm &&
                <ReactDataGrid
                    idProperty="id"
                    columns={columns}
                    dataSource={domains}
                    style={gridStyle}
                />
            }

            {
                showForm &&
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="mt-2 space-y-6">
                        <div>
                            <div className="tw-form-section-wrapper">
                                <div className="md:grid md:grid-cols-3 md:gap-6">
                                    <div className="md:col-span-1">
                                        <h3 className="tw-form-section-heading">Users</h3>
                                        {/* <p className="tw-form-section-subheading">
                                            This will be the main url for the website.
                                        </p> */}
                                    </div>

                                    <div className="mt-5 md:mt-0 md:col-span-2">
                                        <div className="space-y-6">


	                                        <div>
		                                        <label htmlFor="role" className="tw-form-label">
			                                        First name
		                                        </label>
		                                        <div className="relative">
			                                        <div className="relative flex mt-1 rounded-md shadow-sm">
				                                        <input {...register("first_name", {required: true})}
				                                               className="tw-form-input"
				                                        />
				                                        {errors.first_name && <ErrorSVG />}
			                                        </div>
			                                        {errors.first_name && <p className="tw-form-input-error">Invalid name.</p>}
		                                        </div>
	                                        </div>

	                                        <div>
		                                        <label htmlFor="role" className="tw-form-label">
			                                        Last name
		                                        </label>
		                                        <div className="relative">
			                                        <div className="relative flex mt-1 rounded-md shadow-sm">
				                                        <input {...register("last_name", {required: true})}
				                                               className="tw-form-input"
				                                        />
				                                        {errors.last_name && <ErrorSVG />}
			                                        </div>
			                                        {errors.last_name && <p className="tw-form-input-error">Invalid name.</p>}
		                                        </div>
	                                        </div>

	                                        <div>
		                                        <label htmlFor="role" className="tw-form-label">
			                                        Email
		                                        </label>
		                                        <div className="relative">
			                                        <div className="relative flex mt-1 rounded-md shadow-sm">
				                                        <input {...register("email", {required: true})}
				                                               className="tw-form-input"
				                                        />
				                                        {errors.permission && <ErrorSVG />}
			                                        </div>
			                                        {errors.permission && <p className="tw-form-input-error">Invalid email.</p>}
		                                        </div>
	                                        </div>

	                                        <div>
		                                        <label htmlFor="role" className="tw-form-label">
			                                        Phone
		                                        </label>
		                                        <div className="relative">
			                                        <div className="relative flex mt-1 rounded-md shadow-sm">
				                                        <input {...register("phone", {required: true})}
				                                               className="tw-form-input"
				                                        />
				                                        {errors.phone && <ErrorSVG />}
			                                        </div>
			                                        {errors.phone && <p className="tw-form-input-error">Invalid phone.</p>}
		                                        </div>
	                                        </div>

											<div>
												<label htmlFor="role" className="tw-form-label">Role</label>
												<div className="relative">
													<div className="relative flex mt-1 rounded-md shadow-sm">
														<select {...register("role", { required: true })}
															className="tw-form-select"

														>
															{roles.map((role) =>
																<option key={role.id} value={role.id}>{role.name}</option>
															)}
														</select>
													</div>
												</div>
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="tw-form-button-wrapper">
                                <button type="button" className="tw-btn tw-btn-white" onClick={() => setShowForm(false)}>Cancel</button>
                                <button type="submit" className="tw-btn tw-btn-indigo">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            }

        </div>
    )
}




if (document.getElementById('user')) {
    render(<ToastProvider autoDismiss="true"><User /></ToastProvider>, document.getElementById('user'))
}
