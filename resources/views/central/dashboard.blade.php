@extends('layouts.central.logged-in')

@section('main')
    <div class="max-w-6xl mx-auto px-4 sm:px-6 lg:px-8">
        <h2 class="text-lg leading-6 font-medium text-gray-900">Overview</h2>
        <div class="mt-2 grid grid-cols-1 gap-5 sm:grid-cols-2 lg:grid-cols-3" x-max="1">
            <x-metric-card icon="heroicon-o-scale" heading="Recent Accounts" metric="12" route="/admin/sales" />
            <x-metric-card icon="heroicon-o-scale" heading="Total Accounts" metric="129" route="/admin/sales" />
            <x-metric-card icon="heroicon-o-scale" heading="Total Sales" metric="Rs. 30,659.45" route="/admin/sales" />
        </div>
    </div>

    <h2 class="max-w-6xl mx-auto mt-8 px-4 text-lg leading-6 font-medium text-gray-900 sm:px-6 lg:px-8">
        Recent activity
    </h2>

    <div class="block">
        <div class="max-w-6xl mx-auto px-4 sm:px-6 lg:px-8">
            <div class="flex flex-col mt-2">
                <div class="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead>
                        <tr>
                            <th class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Domain
                            </th>
                            <th class="hidden px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider md:block">
                                Status
                            </th>
                            <th class="px-6 py-3 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Date
                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200" x-max="1">

                        <tr class="bg-white">
                            <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                <div class="flex">
                                    <a href="#" class="group inline-flex space-x-2 truncate text-sm">
                                        <p class="text-gray-500 truncate group-hover:text-gray-900">
                                            active.crm.test
                                        </p>
                                    </a>
                                </div>
                            </td>
                            <td class="hidden px-6 py-4 whitespace-nowrap text-sm text-gray-500 md:block">
                                <span class="tw-marker bg-green-100 text-green-800">
                                    Active
                                </span>
                            </td>
                            <td class="px-6 py-4 text-right whitespace-nowrap text-sm text-gray-500">
                                <time datetime="2021-02-11">Feb 11, 2020</time>
                            </td>
                        </tr>

                        <tr class="bg-white">
                            <td class="max-w-0 w-full px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                <div class="flex">
                                    <a href="#" class="group inline-flex space-x-2 truncate text-sm">
                                        <p class="text-gray-500 truncate group-hover:text-gray-900">
                                            test.crm.dev
                                        </p>
                                    </a>
                                </div>
                            </td>
                            <td class="hidden px-6 py-4 whitespace-nowrap text-sm text-gray-500 md:block">
                                <span class="tw-marker bg-yellow-100 text-yellow-800">
                                    Suspended
                                </span>
                            </td>
                            <td class="px-6 py-4 text-right whitespace-nowrap text-sm text-gray-500">
                                <time datetime="2020-07-05">July 5, 2020</time>
                            </td>
                        </tr>

                        </tbody>
                    </table>

                    @include('components.pagination')
                </div>
            </div>
        </div>
    </div>
@endsection
