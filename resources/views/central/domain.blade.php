@extends('layouts.central.logged-in')

@section('main')
    <div class="block">
        <div class="max-w-6xl mx-auto px-4 sm:px-6 lg:px-8">
            <div id="domain"></div>
        </div>
    </div>
@endsection
