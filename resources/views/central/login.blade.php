@extends('layouts.central.logged-out')

@section('main')
    <div class="min-h-screen bg-gray-100 flex flex-col justify-center py-12 sm:px-6 lg:px-8">
        <div class="sm:mx-auto sm:w-full sm:max-w-md">
            <h2 class="text-center text-3xl font-extrabold text-gray-900">
                Sign in to your account
            </h2>
        </div>

        <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
            <div class="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
                <x-form action="{{ route('login') }}" class="space-y-6">
                    <div>
                        <x-label for="email" class="tw-form-label" />
                        <x-input name="email" type="email" autocomplete="email" required class="tw-form-input" />
                    </div>

                    <div>
                        <x-label for="password" class="tw-form-label" />
                        <x-input name="password" type="password" autocomplete="current-password" required class="tw-form-input" />
                    </div>

                    <div>
                        <button type="submit" class="tw-btn tw-btn-indigo w-full">
                            Sign in
                        </button>
                    </div>
                </x-form>
            </div>
        </div>
    </div>
@endsection
