@extends('layouts.central.logged-in')

@section('main')
	<div class="block">
		<div class="max-w-6xl px-4 mx-auto sm:px-6 lg:px-8">
			<div id="sms-template"></div>
		</div>
	</div>
@endsection
