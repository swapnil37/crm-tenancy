<div class="px-2 space-y-1" x-data="{iswebhookapi: false}">
	<a href="{{ route('dashboard') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'dashboard') ? 'active' : '' }}">
		<x-heroicon-o-home /> Home
	</a>

	<a href="{{ route('domains.index') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'domains') ? 'active' : '' }}">
		<x-heroicon-o-globe-alt /> Domains
	</a>

	<a href="#" class="tw-admin-sidebar-nav-item">
		<x-heroicon-o-credit-card /> Billing
	</a>

	<a href="#" class="tw-admin-sidebar-nav-item">
		<x-heroicon-o-presentation-chart-line /> Reports
	</a>

	<a href="#" class="tw-admin-sidebar-nav-item">
		<x-heroicon-o-clock /> Logs
	</a>

	<a href="{{ route('roles.index') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'roles') ? 'active' : '' }}">
		<x-heroicon-o-adjustments /> Roles
	</a>

	<a href="{{ route('users') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'users') ? 'active' : '' }}">
		<x-heroicon-o-users /> Users
	</a>

	<a href="#" @click.prevent="iswebhookapi = !iswebhookapi" class="w-full tw-admin-sidebar-nav-item">
		<span class="flex items-center">
			<x-heroicon-o-document-text /> Templates </span>
		<span class="flex">
			<svg class="justify-end w-2 h-2" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path x-show="!iswebhookapi" d="M9 5L16 12L9 19" stroke="currentColor" stroke-width="2"
							stroke-linecap="round" stroke-linejoin="round" style="display: none;"></path>
				<path x-show="iswebhookapi" d="M19 9L12 16L5 9" stroke="currentColor" stroke-width="2"
							stroke-linecap="round" stroke-linejoin="round"></path>
			</svg>
		</span>
	</a>
	<div x-show="iswebhookapi" class="bg-gray-0">
		<a href="{{ route('email-templates') }}" class="tw-sub-menu-link {{ str_starts_with(Route::currentRouteName(), 'email-templates') ? 'active' : '' }}">
			<span class="pr-2"><x-heroicon-o-mail class="w-4 h-4"/></span> Email
		</a>
		<a href="{{ route('sms-templates') }}" class="tw-sub-menu-link {{ str_starts_with(Route::currentRouteName(), 'sms-templates') ? 'active' : '' }}">
			<span class="pr-2"><x-heroicon-o-chat-alt class="w-4 h-4"/></span> SMS</a>
		<a href="{{ route('whatsapp-templates') }}" class="tw-sub-menu-link {{ str_starts_with(Route::currentRouteName(), 'whatsapp-templates') ? 'active' : '' }}">
			<span class="pr-2"><x-heroicon-o-chat class="w-4 h-4"/></span> WhatsApp</a>
	</div>
</div>

<div class="pt-6 mt-6">
	<div class="px-2 space-y-1">

		<a href="{{ route('user.notification') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'user.notification') ? 'active' : '' }}">
			<x-heroicon-o-speakerphone /> Notifications
		</a>

		<a href="{{ route('user.setting') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'user.setting') ? 'active' : '' }}">
			<x-heroicon-o-cog /> Settings
		</a>

		<a href="#" class="tw-admin-sidebar-nav-item" @click.prevent="document.getElementById('logout-form').submit()">
			<x-heroicon-o-logout /> Logout

			<form id="logout-form" method="POST" action="{{ route('logout') }}">
				@csrf
			</form>
		</a>
	</div>
</div>
