<!-- Search bar -->
<div class="flex justify-between flex-1 px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8">
    <div class="flex flex-1">
        <form class="flex w-full md:ml-0" action="#" method="GET">
            <label for="search_field" class="sr-only">Search</label>
            <div class="relative w-full text-gray-400 focus-within:text-gray-600">
                <div class="absolute inset-y-0 left-0 flex items-center pointer-events-none" aria-hidden="true">
                    <x-heroicon-s-search class="w-5 h-5" />
                </div>
                <input id="search_field"
                       name="search_field"
                       class="block w-full h-full py-2 pl-8 pr-3 text-gray-900 placeholder-gray-500 border-transparent focus:outline-none focus:ring-0 focus:border-transparent sm:text-sm"
                       placeholder="Global search"
                       type="search">
            </div>
        </form>
    </div>

    <div class="flex items-center ml-4 md:ml-6">
        <button class="p-1 text-gray-400 bg-white rounded-full hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-cyan-500">
            <span class="sr-only">View notifications</span>
            <x-heroicon-o-bell class="w-6 h-6" />
        </button>

        <!-- Profile dropdown -->
        <div x-data="{ open: false }"
             @keydown.escape.stop="open = false"
             @click.away="open = false"
             class="relative ml-3">

            <div>
                <button type="button"
                        class="flex items-center max-w-xs text-sm bg-white rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-cyan-500 lg:p-2 lg:rounded-md lg:hover:bg-gray-50"
                        id="user-menu"
                        @click="open = !open"
                        aria-haspopup="true"
                        x-bind:aria-expanded="open"
                >
                    <img class="w-8 h-8 rounded-full" src="https://images.unsplash.com/photo-1480455624313-e29b44bbfde1?ixlib=rb-1.2.1&amp;ixqx=hbOBeTcR0l&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=2.6&amp;w=256&amp;h=256&amp;q=80" alt="">
                    <span class="hidden ml-3 text-sm font-medium text-gray-700 lg:block">
	                    {{-- <span class="sr-only">Open user menu for </span>{{ auth()->user()->name }} --}}
                    </span>
                    <x-heroicon-s-chevron-down class="flex-shrink-0 hidden w-5 h-5 ml-1 text-gray-400 lg:block" />
                </button>
            </div>

            <div x-show="open"
                 x-transition:enter="transition ease-out duration-100"
                 x-transition:enter-start="transform opacity-0 scale-95"
                 x-transition:enter-end="transform opacity-100 scale-100"
                 x-transition:leave="transition ease-in duration-75"
                 x-transition:leave-start="transform opacity-100 scale-100"
                 x-transition:leave-end="transform opacity-0 scale-95"
                 class="absolute right-0 w-48 py-1 mt-2 origin-top-right bg-white rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
                 role="menu"
                 aria-orientation="vertical"
                 aria-labelledby="user-menu"
                 style="display: none;">

                <a href="{{ route('user.profile') }}" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">Your Profile</a>
                <a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">Settings</a>
                <a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem" @click.prevent="document.getElementById('logout-form').submit()">Logout</a>
            </div>
        </div>
    </div>
</div>
