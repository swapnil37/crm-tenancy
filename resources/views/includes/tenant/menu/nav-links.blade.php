<div class="px-2 space-y-1">
	<a href="{{ route('dashboard') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'dashboard') ? 'active' : '' }}">
		<x-heroicon-o-home /> Home
	</a>

	<h3 class="px-3 pt-4 text-xs font-semibold tracking-wider text-white uppercase" id="teams-headline">Leads</h3>

	<a href="{{ route('leads') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'domains') ? 'active' : '' }}">
		<x-heroicon-o-user-group /> All Leads
	</a>

	<a href="{{ route('domains.index') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'domains') ? 'active' : '' }}">
		<x-heroicon-o-user-remove /> Dead Leads
	</a>

	<a href="{{ route('domains.index') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'domains') ? 'active' : '' }}">
		<x-heroicon-o-calendar /> My Followups
	</a>




</div>

<div class="pt-6 mt-6">
	<div class="px-2 space-y-1">
		<h3 class="px-3 text-xs font-semibold tracking-wider text-white uppercase" id="teams-headline">MARKETING</h3>

		<a href="{{ route('user.notification') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'user.notification') ? 'active' : '' }}">
			<x-heroicon-o-thumb-up /> Campaigns
		</a>

		<a href="{{ route('user.notification') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'user.notification') ? 'active' : '' }}">
			<x-heroicon-o-speakerphone /> Drip Marketing
		</a>

	</div>
</div>

<div class="pt-6 mt-6">
	<div class="px-2 space-y-1">

		<a href="{{ route('user.setting') }}" class="tw-admin-sidebar-nav-item {{ str_starts_with(Route::currentRouteName(), 'user.setting') ? 'active' : '' }}">
			<x-heroicon-o-cog /> Components
		</a>

		<a href="#" class="tw-admin-sidebar-nav-item" @click.prevent="document.getElementById('logout-form').submit()">
			<x-heroicon-o-logout /> Logout

			<form id="logout-form" method="POST" action="{{ route('logout') }}">
				@csrf
			</form>
		</a>
	</div>
</div>
