<x-html>
    <x-slot name="head">
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </x-slot>

    <div x-data="{ open: false }" class="flex h-screen overflow-hidden font-sans antialiased bg-gray-200">
        @include('includes.common.menu.mobile')

        @include('includes.common.menu.desktop')

        <div x-init="$el.focus()" class="flex-1 overflow-auto focus:outline-none" tabindex="0">
            <div class="relative z-10 flex flex-shrink-0 h-16 bg-white border-b border-gray-200 lg:border-none">
                <button class="px-4 text-gray-400 border-r border-gray-200 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-cyan-500 lg:hidden" @click="open = true">
                    <span class="sr-only">Open sidebar</span>
                    <x-heroicon-o-menu-alt-1 class="w-6 h-6" />
                </button>

                @include('includes.common.top-bar')
            </div>

            <main class="relative z-0 flex-1 pb-8 overflow-y-auto">
                <!-- Page header -->
				@if (Route::currentRouteName()=='dashboard')
					<div class="bg-white shadow">
						<div class="px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8">
							<div class="py-6 md:flex md:items-center md:justify-between lg:border-t lg:border-gray-200">
								<div class="flex-1 min-w-0">
									<!-- Profile -->
									<div class="flex items-center">
										<div>
											<div class="flex items-center">
												<h1 class="text-2xl font-bold leading-7 text-gray-900 sm:leading-9 sm:truncate">
													{{ \App\Helpers\Greeting::show() }}
												</h1>
											</div>

											<dl class="flex flex-col mt-6 sm:mt-1 sm:flex-row sm:flex-wrap">
												<dt class="sr-only">Company</dt>
												<dd class="flex items-center text-sm font-medium text-gray-500 capitalize sm:mr-6">
													<svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" x-description="Heroicon name: solid/office-building" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
														<path fill-rule="evenodd" d="M4 4a2 2 0 012-2h8a2 2 0 012 2v12a1 1 0 110 2h-3a1 1 0 01-1-1v-2a1 1 0 00-1-1H9a1 1 0 00-1 1v2a1 1 0 01-1 1H4a1 1 0 110-2V4zm3 1h2v2H7V5zm2 4H7v2h2V9zm2-4h2v2h-2V5zm2 4h-2v2h2V9z" clip-rule="evenodd"></path>
													</svg>
													Edge Technosoft
												</dd>
												<dt class="sr-only">Account status</dt>
												<dd class="flex items-center mt-3 text-sm font-medium text-gray-500 capitalize sm:mr-6 sm:mt-0">
													<svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400" x-description="Heroicon name: solid/check-circle" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
														<path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path>
													</svg>
													Super Admin
												</dd>
											</dl>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif


                <div class="mt-8" x-ref="alpine_block">
                    @yield('main')
                </div>
            </main>
        </div>
    </div>

    <script type="text/javascript">
        function alpineFetch(url, $refs) {
            fetch(url)
                .then(response => response.text())
                .then(html => {
                    $refs.alpine_block.innerHTML = html
                })
                .catch(error => {
                    console.log(error)
                })
        }
    </script>

	<script type="text/javascript">
		setInterval(function(){
			const status = navigator.onLine ? 'true' : 'false';
			console.log(status)
		}, 5000);
	</script>

    <script src="{{ mix('js/app.js') }}"></script>
</x-html>
