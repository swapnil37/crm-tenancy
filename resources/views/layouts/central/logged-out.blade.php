<x-html>
    <x-slot name="head">
        <link href="{{ global_asset('css/app.css') }}" rel="stylesheet">
    </x-slot>

    <div class="antialiased font-sans bg-gray-200">
        @yield('main')
    </div>
</x-html>
