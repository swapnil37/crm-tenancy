<x-html>
    <x-slot name="head">
        <link href="{{ global_asset('css/app.css') }}" rel="stylesheet">
    </x-slot>

    <div class="font-sans antialiased bg-gray-200">
        @yield('main')
    </div>
</x-html>
