<div class="relative z-10 flex-shrink-0 flex h-16 bg-white border-b border-gray-200 lg:hidden">
	<button @click.stop="sidebarOpen = true"
					class="px-4 border-r border-gray-200 text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-purple-500 lg:hidden">
		<span class="sr-only">Open sidebar</span>
		<svg class="h-6 w-6" x-description="Heroicon name: outline/menu-alt-1" xmlns="http://www.w3.org/2000/svg"
				 fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
			<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h8m-8 6h16"></path>
		</svg>
	</button>

	<div class="flex-1 flex justify-between px-4 sm:px-6 lg:px-8">
		<div class="flex-1 flex">
			<form class="w-full flex md:ml-0" action="#" method="GET">
				<label for="search_field" class="sr-only">Search</label>
				<div class="relative w-full text-gray-400 focus-within:text-gray-600">
					<div class="absolute inset-y-0 left-0 flex items-center pointer-events-none">
						@include('partials.svg.icon-solid-search')
					</div>
					<input id="search_field" name="search_field" class="tw-sidebar-search" placeholder="Search" type="search">
				</div>
			</form>
		</div>

		<div class="flex items-center">
			<!-- Profile dropdown -->
			<div x-data="{ open: false }"
					 @keydown.escape.stop="open = false"
					 @click.away="open = false"
					 class="ml-3 relative">
				<div>
					<button type="button"
									class="max-w-xs bg-white flex items-center text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500"
									id="user-menu" @click="open = !open" aria-haspopup="true" x-bind:aria-expanded="open">
						<span class="sr-only">Open user menu</span>
						<img class="h-8 w-8 rounded-full"
								 src="https://images.unsplash.com/photo-1502685104226-ee32379fefbe?ixlib=rb-1.2.1&amp;ixqx=hbOBeTcR0l&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=2&amp;w=256&amp;h=256&amp;q=80"
								 alt="">
					</button>
				</div>

				<div x-show="open"
						 x-transition:enter="transition ease-out duration-100"
						 x-transition:enter-start="transform opacity-0 scale-95"
						 x-transition:enter-end="transform opacity-100 scale-100"
						 x-transition:leave="transition ease-in duration-75"
						 x-transition:leave-start="transform opacity-100 scale-100"
						 x-transition:leave-end="transform opacity-0 scale-95"
						 class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 divide-y divide-gray-200 focus:outline-none"
						 role="menu"
						 aria-orientation="vertical"
						 aria-labelledby="user-menu">
					<div class="py-1" role="none">
						<a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
							 role="menuitem">View profile</a>
						<a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
							 role="menuitem">Settings</a>
						<a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
							 role="menuitem">Notifications</a>
					</div>
					<div class="py-1" role="none">
						<a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
							 role="menuitem">Get desktop app</a>
						<a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
							 role="menuitem">Support</a>
					</div>
					<div class="py-1" role="none">
						<a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
							 role="menuitem">Logout</a>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>
