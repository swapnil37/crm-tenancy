<div class="space-y-1">
	<h3 class="px-3 text-xs font-semibold tracking-wider text-gray-500 uppercase" id="teams-headline">NAVIGATION</h3>
	<a href="#" class="tw-link-menu-sidebar">
		<x-heroicon-o-home class="w-4 h-4 mr-4 truncate" />
		Dashboard
	</a>
</div>
