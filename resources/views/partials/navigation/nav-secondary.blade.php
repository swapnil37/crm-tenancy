<div class="mt-8">
	<h3 class="px-3 text-xs font-semibold tracking-wider text-gray-500 uppercase" id="teams-headline">LEADS</h3>

	<div class="mt-1 space-y-1" role="group" aria-labelledby="teams-headline">
		<a href="#" class="tw-comp-menu-link">
			<x-heroicon-o-user-group class="w-4 h-4 mr-4 truncate" />
			<span class="truncate">All Leads</span>
		</a>

		<a href="#"
			 class="tw-comp-menu-link">
			<x-heroicon-o-user-remove class="w-4 h-4 mr-4 truncate" />
			<span class="truncate">Dead Leads</span>
		</a>

		<a href="#"
			 class="tw-comp-menu-link">
			<x-heroicon-o-calendar class="w-4 h-4 mr-4 truncate" />
			<span class="truncate">My Follow-ups</span>
		</a>
	</div>
</div>

<div class="mt-8">
	<h3 class="px-3 text-xs font-semibold tracking-wider text-gray-500 uppercase" id="teams-headline">MARKETING</h3>

	<div class="mt-1 space-y-1" role="group" aria-labelledby="teams-headline">
		<a href="/campaign"
			 class="tw-comp-menu-link">
			<x-heroicon-o-thumb-up class="w-4 h-4 mr-4 truncate" />
			<span class="truncate">Campaigns</span>
		</a>

		<a href="#"
			 class="tw-comp-menu-link">
			<x-heroicon-o-speakerphone class="w-4 h-4 mr-4 truncate" />
			<span class="truncate">Drip Marketing</span>
		</a>
	</div>
</div>

<div class="mt-8">
	<div class="mt-1 space-y-1" role="group" aria-labelledby="teams-headline">
		<a href="/component"
			 class="tw-comp-menu-link">
			<x-heroicon-o-cog class="w-4 h-4 mr-4 truncate" />
			<span class="truncate">Components</span>
		</a>
	</div>
</div>

