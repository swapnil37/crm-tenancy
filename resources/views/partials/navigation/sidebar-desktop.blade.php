<!-- Static sidebar for desktop -->
<div class="hidden lg:flex lg:flex-shrink-0">
	<div class="flex flex-col w-64 border-r border-gray-200 pt-5 pb-4 bg-gray-100">
		<div class="flex items-center flex-shrink-0 px-6">
			<img class="h-8 w-auto" src="https://crm.collegevidya.com/assets/images/logo.png" alt="Workflow">
		</div>

		<div class="h-0 flex-1 flex flex-col overflow-y-auto">
			<!-- User account dropdown -->
			<div x-data="{ open: false }"
					 @keydown.escape.stop="open = false"
					 @click.away="open = false"
					 class="px-3 mt-6 relative inline-block text-left">
				<div>
					<button type="button"
									class="group w-full bg-gray-100 rounded-md px-3.5 py-2 text-sm font-medium text-gray-700 hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500"
									id="options-menu"
									@click="open = !open"
									x-bind:aria-expanded="open">
						<span class="flex w-full justify-between items-center">
							<span class="flex min-w-0 items-center justify-between space-x-3">
								<img class="w-10 h-10 bg-gray-300 rounded-full flex-shrink-0"
										 src="https://images.unsplash.com/photo-1502685104226-ee32379fefbe?ixlib=rb-1.2.1&amp;ixqx=hbOBeTcR0l&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=3&amp;w=256&amp;h=256&amp;q=80"
										 alt="">
								<span class="flex-1 min-w-0">
									<span class="text-gray-900 text-sm font-medium truncate">{{ auth()->user()->name }}</span>
									<span class="text-gray-500 text-sm truncate">Manager</span>
								</span>
							</span>

							@include('partials.svg.icon-solid-selector')
						</span>
					</button>
				</div>

				<div x-show="open"
						 x-transition:enter="transition ease-out duration-100"
						 x-transition:enter-start="transform opacity-0 scale-95"
						 x-transition:enter-end="transform opacity-100 scale-100"
						 x-transition:leave="transition ease-in duration-75"
						 x-transition:leave-start="transform opacity-100 scale-100"
						 x-transition:leave-end="transform opacity-0 scale-95"
						 class="z-10 mx-3 origin-top absolute right-0 left-0 mt-1 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 divide-y divide-gray-200 focus:outline-none"
						 role="menu"
						 aria-orientation="vertical"
						 aria-labelledby="options-menu"
						 style="display: none"
				>

					<div class="py-1" role="none">
						<a href="{{ route('user.profile') }}" class="tw-link-menu-sidebar" role="menuitem">View profile</a>
						<a href="#" class="tw-link-menu-sidebar" role="menuitem">Settings</a>
						<a href="#" class="tw-link-menu-sidebar" role="menuitem">Notifications</a>
					</div>
					<div class="py-1" role="none">
						<a href="#" class="tw-link-menu-sidebar" role="menuitem">Get desktop app</a>
						<a href="#" class="tw-link-menu-sidebar" role="menuitem">Support</a>
					</div>
					<div class="py-1" role="none">
						<x-logout class="tw-link-menu-sidebar" />
					</div>
				</div>

			</div>

			<!-- Sidebar Search -->
		@include('partials.navigation.sidebar-search')

		<!-- Navigation -->
			<nav class="px-3 mt-6">
				<!-- Primary navigation -->
			@include('partials.navigation.nav-primary')

			<!-- Secondary navigation -->
				@include('partials.navigation.nav-secondary')
			</nav>
		</div>
	</div>
</div>

