<div x-show="sidebarOpen" class="lg:hidden">
	<div class="fixed inset-0 flex z-40">

		<div @click="sidebarOpen = false" x-show="sidebarOpen"
				 x-transition:enter="transition-opacity ease-linear duration-300"
				 x-transition:enter-start="opacity-0"
				 x-transition:enter-end="opacity-100"
				 x-transition:leave="transition-opacity ease-linear duration-300"
				 x-transition:leave-start="opacity-100"
				 x-transition:leave-end="opacity-0"
				 class="fixed inset-0">
			<div class="absolute inset-0 bg-gray-600 opacity-75"></div>
		</div>

		<div x-show="sidebarOpen"
				 x-transition:enter="transition ease-in-out duration-300 transform"
				 x-transition:enter-start="-translate-x-full"
				 x-transition:enter-end="translate-x-0"
				 x-transition:leave="transition ease-in-out duration-300 transform"
				 x-transition:leave-start="translate-x-0"
				 x-transition:leave-end="-translate-x-full"
				 class="relative flex-1 flex flex-col max-w-xs w-full pt-5 pb-4 bg-white">

			@include('partials.navigation.mobile-close-btn')

			<div class="flex-shrink-0 flex items-center px-4">
				<img class="h-8 w-auto" src="https://tailwindui.com/img/logos/workflow-logo-purple-500-mark-gray-700-text.svg"
						 alt="Workflow">
			</div>

			<div class="mt-5 flex-1 h-0 overflow-y-auto">
				<nav class="px-2">
					<!-- Primary navigation -->
				@include('partials.navigation.nav-primary')

				<!-- Secondary navigation -->
					@include('partials.navigation.nav-secondary')
				</nav>
			</div>
		</div>

		<div class="flex-shrink-0 w-14" aria-hidden="true">
			<!-- Dummy element to force sidebar to shrink to fit close icon -->
		</div>

	</div>
</div>
