@extends('layouts.tenant.logged-in')

@section('main')
    <div class="max-w-6xl px-4 mx-auto sm:px-6 lg:px-8">
        <h2 class="text-lg font-medium leading-6 text-gray-900">Overview</h2>
        <div class="grid grid-cols-1 gap-5 mt-2 sm:grid-cols-2 lg:grid-cols-3">
            <!-- Card -->

            <div class="overflow-hidden bg-white rounded-lg shadow">
                <div class="p-5">
                    <div class="flex items-center">
                        <div class="flex-shrink-0">
                            <x-heroicon-o-scale class="w-6 h-6 text-gray-400" />
                        </div>
                        <div class="flex-1 w-0 ml-5">
                            <dl>
                                <dt class="text-sm font-medium text-gray-500 truncate">
                                    Account balance
                                </dt>
                                <dd>
                                    <div class="text-lg font-medium text-gray-900">
                                        $30,659.45443
                                    </div>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="px-5 py-3 bg-gray-50">
                    <div class="text-sm">
                        <a href="#" class="font-medium text-cyan-700 hover:text-cyan-900">
                            View all
                        </a>
                    </div>
                </div>
            </div>

            <!-- More items... -->
        </div>
    </div>

    <h2 class="max-w-6xl px-4 mx-auto mt-8 text-lg font-medium leading-6 text-gray-900 sm:px-6 lg:px-8">
        Recent activity
    </h2>

    <!-- Activity list (smallest breakpoint only) -->
    <div class="shadow sm:hidden">
        <ul class="mt-2 overflow-hidden divide-y divide-gray-200 shadow sm:hidden">
            <li>
                <a href="#" class="block px-4 py-4 bg-white hover:bg-gray-50">
					<span class="flex items-center space-x-4">
						<span class="flex flex-1 space-x-2 truncate">
							<x-heroicon-s-cash class="flex-shrink-0 w-5 h-5 text-gray-400" />
							<span class="flex flex-col text-sm text-gray-500 truncate">
								<span class="truncate">Payment to Molly Sanders</span>
								<span><span class="font-medium text-gray-900">$20,000</span> USD</span>
								<time datetime="2020-07-11">July 11, 2020</time>
							</span>
						</span>
						<x-heroicon-s-chevron-right class="flex-shrink-0 w-5 h-5 text-gray-400" />
					</span>
                </a>
            </li>

            <!-- More items... -->
        </ul>

        <nav class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200" aria-label="Pagination">
            <div class="flex justify-between flex-1">
                <a href="#" class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md hover:text-gray-500">
                    Previous
                </a>
                <a href="#" class="relative inline-flex items-center px-4 py-2 ml-3 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md hover:text-gray-500">
                    Next
                </a>
            </div>
        </nav>
    </div>

    <!-- Activity table (small breakpoint and up) -->
    <div class="hidden sm:block">
        <div class="max-w-6xl px-4 mx-auto sm:px-6 lg:px-8">
            <div class="flex flex-col mt-2">
                <div class="min-w-full overflow-hidden overflow-x-auto align-middle shadow sm:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead>
                        <tr>
                            <th class="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase bg-gray-50">
                                Transaction
                            </th>
                            <th class="px-6 py-3 text-xs font-medium tracking-wider text-right text-gray-500 uppercase bg-gray-50">
                                Amount
                            </th>
                            <th class="hidden px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase bg-gray-50 md:block">
                                Status
                            </th>
                            <th class="px-6 py-3 text-xs font-medium tracking-wider text-right text-gray-500 uppercase bg-gray-50">
                                Date
                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                        <tr class="bg-white">
                            <td class="w-full px-6 py-4 text-sm text-gray-900 max-w-0 whitespace-nowrap">
                                <div class="flex">
                                    <a href="#" class="inline-flex space-x-2 text-sm truncate group">
                                        <x-heroicon-s-cash class="flex-shrink-0 w-5 h-5 text-gray-400 group-hover:text-gray-500" />
                                        <p class="text-gray-500 truncate group-hover:text-gray-900">
                                            Payment to Molly Sanders
                                        </p>
                                    </a>
                                </div>
                            </td>
                            <td class="px-6 py-4 text-sm text-right text-gray-500 whitespace-nowrap">
                                <span class="font-medium text-gray-900">$20,000 </span>
                                USD
                            </td>
                            <td class="hidden px-6 py-4 text-sm text-gray-500 whitespace-nowrap md:block">
                                <span class="text-green-800 bg-green-100 tw-marker">
                                    success
                                </span>
                            </td>
                            <td class="px-6 py-4 text-sm text-right text-gray-500 whitespace-nowrap">
                                <time datetime="2020-07-11">July 11, 2020</time>
                            </td>
                        </tr>

                        <!-- More items... -->
                        </tbody>
                    </table>

                    <x-pagination cursor-start="1" cursor-end="10" total="20" />
                </div>
            </div>
        </div>
    </div>
@endsection
