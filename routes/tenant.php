<?php

declare(strict_types=1);

use App\Http\Controllers\ResolutionItemController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;
use App\Http\Controllers\Tenant\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/



Route::middleware([
    'web',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])->group(function () {

	Route::get('/', function () {
		$path = Auth::user() ? 'dashboard' : 'login';
		return redirect()->to($path);
	});
	Route::view('login', 'tenant.auth.login')->name('login');
	Route::post('login', [LoginController::class, 'authenticate'])->name('login');

	Route::middleware(['auth'])->group(function () {
		Route::prefix('user')->group(function () {
			Route::view('dashboard', 'tenant.dashboard')->name('dashboard');
			Route::view('leads', 'tenant.dashboard')->name('leads');
		});
	});
});
