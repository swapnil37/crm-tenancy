<?php

use App\Http\Controllers\DomainController;
use App\Http\Controllers\EmailTemplateController;
use App\Http\Controllers\SMSTemplateController;
use App\Http\Controllers\WhatsappTemplateController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SettingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::view('admin/login', 'central.login')->name('central.login');

Route::prefix('user')->group(function () {
    Route::view('dashboard', 'central.dashboard')->name('dashboard');
    Route::get('profile', [ProfileController::class, 'index'])->name('user.profile');
    Route::patch('profile/update', [ProfileController::class, 'update']);
    Route::get('settings', [SettingController::class, 'index'])->name('user.setting');
    Route::patch('settings/update', [SettingController::class, 'update']);
	Route::view('notification', 'central.notification')->name('user.notification');
	Route::view('users', 'central.user')->name('users');
	Route::view('email-templates', 'central.email-template')->name('email-templates');
	Route::view('sms-templates', 'central.sms-template')->name('sms-templates');
	Route::view('whatsapp-templates', 'central.whatsapp-template')->name('whatsapp-templates');
	Route::get('domains/edit/{id}', [DomainController::class, 'edit'])->name('user.domains.edit');
	Route::get('email-template/edit/{id}', [EmailTemplateController::class, 'edit'])->name('user.email-template.edit');
	Route::post('email-template/delete/', [EmailTemplateController::class, 'destroy'])->name('user.email-template.delete');
	Route::get('sms-template/edit/{id}', [SMSTemplateController::class, 'edit'])->name('user.sms-template.edit');
	Route::post('sms-template/delete/', [SMSTemplateController::class, 'destroy'])->name('user.sms-template.delete');
	Route::get('whatsapp-template/edit/{id}', [WhatsappTemplateController::class, 'edit'])->name('user.whatsapp-template.edit');
	Route::post('whatsapp-template/delete/', [WhatsappTemplateController::class, 'destroy'])->name('user.whatsapp-template.delete');


    Route::resources([
        'domains'     => DomainController::class,
        'roles'       => RoleController::class,
        'permissions' => PermissionController::class,
		'email-template'=>EmailTemplateController::class,
		'sms-template'=>SMSTemplateController::class,
		'whatsapp-template'=>WhatsappTemplateController::class,

    ]);
});
